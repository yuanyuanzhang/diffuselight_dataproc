
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 

# In[2]:


get_ipython().magic('matplotlib inline')
import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.3)
from astropy.table import Table


# # Random points

# In[3]:


ini_file= 'rand_y1.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir_rand=config.get('input', 'input_dir')
catalog_rand=config.get('input', 'catalog_file')
flag_files_rand=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir_rand=config.get('output', 'save_dir')
grid_output_dir_rand=config.get('grid_output', 'save_dir')
rand=pyfits.open(catalog_rand)[1].data
flags_rand=np.zeros(len(rand))+1
indices_rand=np.arange(len(rand))


cc=0
for file_flag in flag_files_rand:
    #if cc==1:
    #    continue
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        print(sum(flag))
        if len(flag) == len(flags_rand):
            flags_rand=flags_rand*flag
    cc=cc+1
ind_rand, =np.where( (flags_rand > 0.5) & (flags_rand<1.5) )
print(len(ind_rand), len(indices_rand))



# #### Removing indices that has no profiles

# In[ ]:


####
second_path_rand = 'fermigrid_rand/'
dat_type=[('distance', float), ('mean', float), ('median', float)]

to_be_removed = []

for jji, jj in enumerate(ind_rand):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    file_jj_r=input_dir_rand+second_path_rand+'object_%i/profile_%i_r.txt'%(jj,jj)
    if os.path.isfile(file_jj_r):
        continue
    else:
        print('there is no profile for the cluster with index '+str(jj))
        to_be_removed.append(jj)

# remove the clusters indices that there is no profiles
for rem in to_be_removed:
    ind_rand = np.delete(ind_rand, list(ind_rand).index(rem))
    
print('total number of clusters to use: '+str(len(ind_rand)))
        


# #### The funtion below is for getting the mean random profile for a given redshift

# In[4]:


def z_to_mean_rand(ind_rand, nbins, input_dir, second_path):
    
    ####
    input_dir='/home/hillysson/diffuselight_dataproc/dlight/'
    second_path='fermigrid_rand/'
    dat_type=[('distance', float), ('mean', float), ('median', float)]
    data_temp=np.loadtxt(input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind_rand[10],ind_rand[10]), dtype=dat_type)

    
    ncluster=len(ind_rand)
    yy_r=np.zeros([nbins, ncluster])+99999
    xx=np.zeros([nbins, ncluster])+99999
    

    for jji, jj in enumerate(ind_rand):

        file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)
        
        if os.path.isfile(file_jj_r):
            data_r=np.loadtxt(file_jj_r, dtype=dat_type)

            if len(data_r['distance'])==nbins:
                yy_r[:, jji]=data_r['mean']#*yyfactor
                xx[:, jji]=data_temp['distance']#*xxfactor

                

    ## jacknife sampling
    from astropy.io.fits import Column

    # getting the center position of rdmp clusters
    #coord_rdmp=np.transpose( np.array([rdmp[ind]['ra'], rdmp[ind]['dec']]) )
    ncen = 40

    if os.path.isfile('phy_centers.txt') == 0: #this is false, so the code is jumping this part
        km = kmeans_sample(coord_rdmp, ncen, maxiter=100, tol=1.0e-5)
        print("found centers:",km.centers)
        print("converged?",km.converged)
        np.savetxt('phy_centers.txt', km.centers)
    centers=np.loadtxt('phy_centers.txt')

    # find the rdmp cluster closest to the center of the region:
    coord_rand=np.transpose( np.array([rand[ind_rand]['ra'], rand[ind_rand]['dec']]) )
    labels_rand=kmeans_radec.find_nearest(coord_rand, centers)
    #print(labels_rand)
    print(str(len(labels_rand))+' random points')
    print('  ')


    len_kmeans=centers.shape[0] # 40, number of regions
    dir_ind='/home/hillysson/diffuselight_dataproc/dlight/output/redmapper_y1_jacknife_sub_profiles/'
    os.system('mkdir '+dir_ind)
    

    c0=Column(name='Jacknife_index', format='K')
    c1=Column(name='x_axis', format='%iE'%nbins)
    c2=Column(name='mean', format='%iE'%nbins)
    cols = pyfits.ColDefs([c0, c1, c2])
    hdu = pyfits.BinTableHDU.from_columns(cols, nrows=len_kmeans)

    for ii in range(len_kmeans): # binning in kmean regions

        # subtracting clusters that belong to this region
        ind_rand,=np.where( (labels_rand != ii) )    
        print('region %i:'%(ii))
        print(str(len(labels_rand)-len(ind_rand))+' cluster(s) were removed')
        print(str(len(ind_rand))+' cluster(s) were used')
        print('')

        yyrmean_ind=np.zeros(nbins)    
        for jj in range(nbins): # binning in radius        
            temp_r_mean =yy_r[jj, :]        
            ind_temp, =np.where((temp_r_mean < 30000) & (temp_r_mean >-30000) &                                 (labels_rand != ii) )        
            arr=temp_r_mean[ind_temp]
            yyrmean_ind[jj]=np.mean(arr)

        hdu.data[ii]['Jacknife_index']=ii
        hdu.data[ii]['x_axis']=xx.T[0]
        hdu.data[ii]['mean']=yyrmean_ind
    
    rand_mean, err=jacknife_statistics(hdu.data['mean'])
    xx_arr = hdu.data['x_axis']
    
    
    return xx_arr, rand_mean

def jacknife_statistics(dat1, dat2 = np.zeros(1)):

    njack, narray = dat1.shape
    if dat1.shape != dat2.shape:
        dat2 = np.zeros([njack, narray]) # alternatively dat2 is 0
        print('no second array, using 0')
    mean = np.zeros(narray); err = np.zeros(narray)
    err_factor = np.sqrt(njack-1)

    for ii in range(narray):
        bkg = 0 #dat1[:, narray-1] - dat2[:, narray-1] # 0 for default
        mean[ii] = np.mean(dat1[:, ii] - dat2[:, ii]-bkg)
        err[ii] = np.std(dat1[:, ii] - dat2[:, ii] - bkg) * err_factor
    return mean, err


#nbins=359
#xxbins = np.arange(0.0,2000.1, 2000/nbins )
#xx_rand, yy_r_rand =z_to_mean_rand( ind_rand, 359, '/home/hillysson/diffuselight_dataproc/dlight/', 'fermigrid_rand/')
rand=pyfits.open('/home/hillysson/diffuselight_dataproc/dlight/output/redmapper_y1_jacknife_sub_profiles/'+'jacknife_profile_r.fits')[1].data['mean']
xx_rand, yy_r_rand=jacknife_statistics(rand)


# # redMaPPer points

# In[5]:


ini_file= 'redmapper_y1_new.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir_rdmp=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files_rdmp=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir_rdmp=config.get('output', 'save_dir')
grid_output_dir_rdmp=config.get('grid_output', 'save_dir')
rdmp=pyfits.open(catalog)[1].data
flags_rdmp=np.zeros(len(rdmp))+1
indices_rdmp=np.arange(len(rdmp))

str_ngals = ['$\lambda$ = [20;30)',
             '$\lambda$ = [30;45)',
             '$\lambda$ = [45;60)',
             r'$\lambda$ = [60;$\infty$)']
richness_dir = ['richness_20_30/',
                'richness_30_45/',
                'richness_45_60/',
                'richness_60_beyond/']

# getting indices in the flag files
for file_flag in flag_files_rdmp:
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        if len(flag) == len(flags_rdmp):
            flags_rdmp=flags_rdmp*flag

ind1, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 20) & (rdmp['lambda_chisq'] < 30))
print(len(ind1), len(indices_rdmp))
ind2, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 30) & (rdmp['lambda_chisq'] < 45))
print(len(ind2), len(indices_rdmp))
ind3, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 45) & (rdmp['lambda_chisq'] < 60))
print(len(ind3), len(indices_rdmp))
ind4, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 60) & (rdmp['lambda_chisq'] < 40000000))
print(len(ind4), len(indices_rdmp))


# #### Removing indices that has no profiles

# In[6]:


ngal_bins=4
inds = [ind1,ind2,ind3,ind4]
ind = inds[ngal_bins-1]
Ncl = len(ind)

####
second_path_rdmp = 'fermigrid_profiles_new/'
dat_type=[('distance', float), ('mean', float), ('median', float)]

to_be_removed = []

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    file_jj_r=input_dir_rdmp+second_path_rdmp+'object_%i/profile_%i_r.txt'%(jj,jj)
    if os.path.isfile(file_jj_r):
        continue
    else:
        print('there is no profile for the cluster  with index '+str(jj))
        to_be_removed.append(jj)

# remove the clusters indices that there is no profiles
for rem in to_be_removed:
    ind = np.delete(ind, list(ind).index(rem))
    
print('total number of clusters to use: '+str(len(ind)))
        


# #### The funtion below is for getting the cluster profile for a given redshift

# In[83]:


def z_to_mean_rdmp(z_, ind, nbins, input_dir, second_path):
    
    ####
    
    input_dir='/home/hillysson/diffuselight_dataproc/dlight/'
    second_path='fermigrid_profiles_new/'
    dat_type=[('distance', float), ('mean', float), ('median', float)]
    
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind,ind)

    if os.path.isfile(file_jj_r):
        data_r=np.loadtxt(file_jj_r, dtype=dat_type)

        #xxfactor=cosmo.kpc_proper_per_arcmin(z_).value*0.263/60
        #yyfactor=1.0/(xxfactor**2)

        if len(data_r['distance'])==nbins:
            yy_r     = data_r['mean']#*yyfactor
            xx       = data_r['distance']#*xxfactor
    
    return xx, yy_r


# # Subtracting the cluster profile from the random profile

# In[84]:


nbins=359
#xxbins = np.arange(0.0,2000.1, 2000/nbins )
#phy_xx = (xxbins[:-1]+xxbins[1:])/2



import time
import multiprocessing
from multiprocessing import Pool
from scipy.interpolate import interp1d

os.system('mkdir '+save_dir_rdmp+'subtracted_profiles') 
os.system('mkdir '+save_dir_rdmp+'subtracted_profiles/'+richness_dir[ngal_bins-1]) 
dir_sub=save_dir_rdmp+'subtracted_profiles/'+richness_dir[ngal_bins-1]

def sub_profiles(jj):
    
    if os.path.isfile(dir_sub+'subtracted_profile_%i.txt'%(jj)):
        print('subtracted_profile_%i.txt already exists!'%(jj))
        return
        
    else:

        
        start = time.time()
        
        zp_= rdmp['Z_LAMBDA'][jj]

        xx_rdmp, yy_r_rdmp=z_to_mean_rdmp(zp_, jj, nbins, input_dir_rdmp,second_path_rdmp)

        yy_r_sub_     = yy_r_rdmp - yy_r_rand

        
        yy_fact=(1+zp_)**4.0/1.275**4.0
        xx_temp=0.263*cosmo.kpc_proper_per_arcmin(zp_).value/60*xx_rdmp
        xx_grid=0.263*cosmo.kpc_proper_per_arcmin(0.275).value/60*xx_rdmp
        func=interp1d(xx_temp, yy_r_sub_, fill_value = 'extrapolate')
        yy_r_sub=func(xx_grid)*yy_fact
               
        
        

        '''
        fig = plt.figure(figsize=(7,7))  
        plt.subplot(1,1,1)
        ax = fig.add_subplot(1,1,1)
        plt.xscale('log')
        
        temp, =np.where((np.array(yy_r_sub) < 300000) & (np.array(yy_r_sub) >-300000))

        #xx_ =np.array(phy_xx)[temp]
        #rand_ =np.array(yy_r_rand)[temp]
        #rdmp_ =np.array(yy_r_rdmp)[temp]
        #sub_ =np.array(yy_r_sub)[temp]

        #plt.simlogy(xx, yy_r, 'r-', alpha=0.4, label='cluster 911')
        plt.plot(xx_rdmp,yy_r_rand, 'r-', alpha=0.4, label='random')
        plt.plot(xx_rdmp, yy_r_rdmp, '-', color='lime', alpha=0.4, label='redMaPPer')
        plt.plot(xx_rdmp, yy_r_sub_, '-', color='purple', alpha=0.4, label='subtracted')
        plt.plot(xx_grid, yy_r_sub, '-', color='black', alpha=0.4, label='subtracted_grid')
        plt.yscale('symlog')
        plt.ylabel('flux [kpc$^{-2}$]', fontsize=17)
        plt.xlabel('r [kpc]', fontsize=17)
        #plt.xlim(0.0, 0.8)
        #plt.ylim(0.02, 200)
        plt.legend(bbox_to_anchor=(0.53, 0.9), loc=2, fontsize=12,borderaxespad=0.,numpoints=1,scatterpoints=1)
        plt.xticks(fontsize = 0)
        plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
        plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
        plt.yticks(fontsize=15)
        plt.xticks(fontsize=15)
        [ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
        plt.subplots_adjust(wspace=0, hspace=0)
        plt.show()
        '''


        


        '''
        phy_flux_r=[]
        for xxb in range(len(xxbins)-1):
            xxmask = (xx_rdmp>=xxbins[xxb]) * (xx_rdmp<xxbins[xxb+1])

            vec_r=yy_r_sub[xxmask]
            ytemp, =np.where((np.array(vec_r) < 300000) & (np.array(vec_r) >-300000))
            y_non_nan= np.array(vec_r)[ytemp]
            phy_flux_r.append( np.mean(y_non_nan))
        '''
            
        #print(phy_flux_r)
        #print(len(yy_r_sub))
        #print(len(phy_flux_r))

        
        # setting up the varibles
        #xx = phy_xx
        #xx_temp=xx
        #yy_temp=2*np.pi*yy_r_sub*xx

        # insert zero in the beggining of the array
        #yy_temp=np.insert(yy_temp, 0, 0)
        #xx_temp=np.insert(xx_temp, 0, 0)

        # computing the trapezoid area for each radius area
        #a_trapz = []
        #for iii in range(len(phy_xx)):
        #    aa_trapz = np.trapz(yy_temp[iii:iii+2],xx_temp[iii:iii+2])
        #    a_trapz.append(aa_trapz)

        # computing the integrated flux
        #integrated = np.cumsum(a_trapz)
        
        '''
        fig = plt.figure(figsize=(7,7))  
        plt.subplot(1,1,1)
        ax = fig.add_subplot(1,1,1)
        plt.xscale('log')
        
        temp, =np.where((np.array(yy_r_sub) < 300000) & (np.array(yy_r_sub) >-300000))

        xx_ =np.array(phy_xx)#[temp]
        rand_ =np.array(yy_r_rand)#[temp]
        rdmp_ =np.array(yy_r_rdmp)#[temp]
        sub_ =np.array(yy_r_sub)#[temp]

        #plt.simlogy(xx, yy_r, 'r-', alpha=0.4, label='cluster 911')
        plt.plot(xx_,rand_ , 'r-', alpha=0.4, label='random')
        plt.plot(xx_, rdmp_, '-', color='lime', alpha=0.4, label='redMaPPer')
        plt.plot(xx_, sub_, '-', color='purple',lw=3, alpha=0.4, label='subtracted')
        #plt.plot(phy_xx, phy_flux_r, '-', color='black', alpha=0.4, label='sub')
        plt.yscale('symlog')
        plt.ylabel('flux [kpc$^{-2}$]', fontsize=17)
        plt.xlabel('r [kpc]', fontsize=17)
        #plt.xlim(0.0, 0.8)
        #plt.ylim(0.02, 200)
        plt.legend(bbox_to_anchor=(0.53, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
        plt.xticks(fontsize = 0)
        plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
        plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
        plt.yticks(fontsize=15)
        plt.xticks(fontsize=15)
        [ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
        plt.subplots_adjust(wspace=0, hspace=0)
        plt.show()
        '''
        
        #z=list(zip(phy_xx, yy_g_sub, yy_g_sub_med, yy_r_sub, yy_r_sub_med, integrated))
        #np.savetxt(dir_sub+'subtracted_profile_%i.txt'%jj, z, fmt='%f, %f, %f, %f, %f, %f', header='# xx, g_mean, g_median, r_mean, r_median, integrated_r')
        z=list(zip(xx_grid, yy_r_sub))
        np.savetxt(dir_sub+'subtracted_profile_%i.txt'%jj, z, fmt='%f, %f', header='# xx, r_mean')
        print(str(list(ind).index(jj))+": %f sec" % (time.time() - start))
        
        return

    
if __name__ == "__main__": 

    pool = Pool(processes=4)
    pool.map(sub_profiles, ind)

#sub_profiles(151)    
#sub_profiles(4688)
#sub_profiles(4701)
#sub_profiles(ind[1])

