import math
import numpy as np
import matplotlib.pyplot as plt
import astropy.io.fits as pyfits
import os
import os.path
from numpy import cos
from astropy.wcs import WCS
from numpy import cos
from numpy import sin
import ConfigParser
import sys

ini_file=sys.argv[1]
config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
bandlist=[ii.strip() for ii in config.get('parameters', 'bands').split(',')]

catalog_file=config.get('input', 'catalog_file')
flag_files=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
input_dir=config.get('grid_input', 'input_dir')
save_dir=config.get('grid_output', 'save_dir')
services_inifile=config.get('grid_input', 'services_inifile')
start_index=int(config.get('parameters', 'start_index'))
batches_num=int(config.get('parameters', 'batches_num'))
batch_len=int(config.get('parameters', 'batchlen'))

cwd = os.getcwd()
os.system('mkdir lists')
list_dec=cwd+'/lists/list_'
input_ini_file = cwd+'/'+ini_file
services_inifile = cwd+'/'+services_inifile


input_catalog=input_dir+catalog_file
catalog=pyfits.open(input_catalog)[1].data
#### take in flags for objects in the catalog
flags=np.zeros(len(catalog))+1
for file_flag in flag_files:
    if os.path.isfile(input_dir+file_flag):
       flag_dat=np.genfromtxt(input_dir+file_flag, dtype=[('index', int), ('flag', int)])
       flag=np.zeros(len(catalog))+1
       flag[flag_dat['index']]=flag_dat['flag']
       flag[0:start_index]= 0
       if len(flag) == len(flags):
          flags=flags*flag
flags[0:start_index]= 0
ind, =np.where( (flags > 0.5) & (flags<1.5))

ct=0
for ii in range(batches_num):
  if ct < len(ind):
    list_name=list_dec+'%i.txt'%ii
    ff = open(list_name, "w")
    for jj in range(batch_len):
        if ct < len(ind):
           print 'Working on batch %i, object %i out of %i'%(ii, ind[ct], len(catalog))
           ff.write('%i \n'%ind[ct])
        ct=ct+1  
    ff.close()
    submit_command='jobsub_submit -G des --memory=2GB --disk=40GB --expected-lifetime=72h --resource-provides=usage_model=OPPORTUNISTIC --OS=SL6 file://coadd_nomask_profile_y6.sh %s %s %s %s %s %s'%(list_name, input_catalog, save_dir, cwd, input_ini_file, services_inifile)
    os.system(submit_command)
    #os.system('bash coadd_mask_profile.sh %s %s %s %s %s %s'%(list_name, input_catalog, save_dir, cwd, ini_file, services_inifile))
    print submit_command
    print 'I have submitted job %i out of %i'%(ii, batches_num)
