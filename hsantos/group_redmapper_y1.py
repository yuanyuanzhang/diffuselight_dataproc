
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 

# In[1]:


import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys


# In[3]:


ini_file= 'redmapper_y1.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir=config.get('output', 'save_dir')
grid_output_dir=config.get('grid_output', 'save_dir')
rdmp=pyfits.open(catalog)[1].data
flags=np.zeros(len(rdmp))+1
indices=np.arange(len(rdmp))


# In[4]:


str_ngals = ['Ngals = [20;30)',
             'Ngals = [30;45)',
             'Ngals = [45;60)',
             r'Ngals = [60;$\infty$)']


# In[6]:


# getting indices in the flag files
for file_flag in flag_files:
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        if len(flag) == len(flags):
            flags=flags*flag
#ind, =np.where( (flags > 0.5) & (flags<1.5) )#& (rdmp['lambda_chisq']>= 40) & (rdmp['lambda_chisq'] < 40000000))
ind1, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 20) & (rdmp['lambda_chisq'] < 30))
print(len(ind1), len(indices))
ind2, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 30) & (rdmp['lambda_chisq'] < 45))
print(len(ind2), len(indices))
ind3, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 45) & (rdmp['lambda_chisq'] < 60))
print(len(ind3), len(indices))
ind4, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 60) & (rdmp['lambda_chisq'] < 40000000))
print(len(ind4), len(indices))


# In[130]:


ngal_bins=4
inds = [ind1,ind2,ind3,ind4]
ind = inds[ngal_bins-1]
Ncl = len(ind)


# In[131]:


####
second_path = 'fermigrid_profiles/'
dat_type=[('distance', float), ('mean', float), ('median', float)]
data_temp=np.loadtxt(input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind[10],ind[10]), dtype=dat_type)
ncluster=len(ind)
nbins=len(data_temp['distance'])#20#399
yy_g=np.zeros([nbins, ncluster])+99999
yy_r=np.zeros([nbins, ncluster])+99999
yy_g_med=np.zeros([nbins, ncluster])+99999
yy_r_med=np.zeros([nbins, ncluster])+99999
xx=data_temp['distance']
ct=0

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    file_jj_g=input_dir+second_path+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)
    #print(os.path.isfile(file_jj_r))
    if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
        data_g=np.loadtxt(file_jj_g, dtype=dat_type)
        data_r=np.loadtxt(file_jj_r, dtype=dat_type)
        #print(len(data_r['distance']))
        #print(data_r[:2])
        

        if len(data_g['distance'])==nbins and len(data_r['distance'])==nbins: # nbins: length of the profile, number of radius bins
            if 1:
                yy_g[:, jji]=data_g['mean']
                yy_g_med[:, jji]=data_g['median']
                yy_r[:, jji]=data_r['mean']
                yy_r_med[:, jji]=data_r['median']
                ct=ct+1
                #print(jji, len(data_g['distance']),len(data_r['distance']))
        else:
            print(jji)

print('total number of points to use:', ct)


# In[132]:


yy=np.zeros(nbins)
for ii in range(nbins):
    print(yy_r[ii, :])
    arr_temp =yy_r[ii, :]
    ind_temp, =np.where((arr_temp < 30000) & (arr_temp >-30000))
    arr=arr_temp[ind_temp]
    yy[ii]=np.mean(arr)


# In[133]:


## jacknife sampling

# getting the center position of rdmp clusters
coord_rdmp=np.transpose( np.array([rdmp[ind]['ra'], rdmp[ind]['dec']]) )
ncen = 10

if os.path.isfile('centers.txt') == 0: #this is false, so the code is jumping this part
    km = kmeans_sample(coord_rdmp, ncen, maxiter=100, tol=1.0e-5)
    print("found centers:",km.centers)
    print("converged?",km.converged)
    np.savetxt('centers.txt', km.centers)
centers=np.loadtxt('centers.txt')

# find the rdmp cluster closest to the center of the region:
labels_rdmp=kmeans_radec.find_nearest(coord_rdmp, centers)


len_kmeans=centers.shape[0] # 40, number of regions
dir_ind=save_dir+'redmapper_y1_jacknife_profiles/'
os.system('mkdir '+dir_ind)

for ii in range(len_kmeans): # binning in kmean regions
    
    # subtracting clusters that belong to a region
    ind_rdmp,=np.where( (labels_rdmp != ii) )
    
    # saving file with the cluster indices of regions not subtracted
    np.savetxt(dir_ind+'jacknife_index_%i.txt'%ii, ind[ind_rdmp], fmt='%i') 
    print('region '+str(ii))
    # print 'number of clusters: '+str(357-len(ind_rdmp))
    # print 
    
    yygmean_ind=np.zeros(nbins)
    yygmed_ind=np.zeros(nbins)
    yyrmean_ind=np.zeros(nbins)
    yyrmed_ind=np.zeros(nbins)
    
    for jj in range(nbins): # binning in radius
        
        temp_g_mean =yy_g[jj, :]
        temp_g_med =yy_g_med[jj, :]
        temp_r_mean =yy_r[jj, :]
        temp_r_med =yy_r_med[jj, :]
        
        ind_temp, =np.where((temp_g_mean < 30000) & (temp_g_mean >-30000) &                             (temp_g_med < 30000) & (temp_g_med >-30000) &                             (temp_r_mean < 30000) & (temp_r_mean >-30000) &                             (temp_r_med < 30000) & (temp_r_med >-30000) &                             #(labels_rdmp != ii) )
                            (labels_rdmp == ii) )
        arr=temp_g_mean[ind_temp]
        yygmean_ind[jj]=np.mean(arr)
        arr=temp_g_med[ind_temp]
        yygmed_ind[jj]=np.mean(arr)
        
        arr=temp_r_mean[ind_temp]
        yyrmean_ind[jj]=np.mean(arr)
        arr=temp_r_med[ind_temp]
        yyrmed_ind[jj]=np.mean(arr)
    z=list(zip(xx, yygmean_ind, yygmed_ind, yyrmean_ind, yyrmed_ind))
    #z=list(zip(xx, yyrmean_ind, yyrmed_ind))
    np.savetxt(dir_ind+'jacknife_profile_%i.txt'%ii, z, fmt='%f, %f, %f, %f, %f', header='# xx, g_mean, g_median, r_mean, r_median')
    #np.savetxt(dir_ind+'jacknife_profile_%i.txt'%ii, z, fmt='%f, %f, %f', header='# xx, r_mean, r_median')


# In[139]:


fig = plt.figure(figsize=(6,6))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)

plt.plot(coord_rdmp.T[0],coord_rdmp.T[1], 'k.', label = 'RM centers')
plt.plot(centers.T[0],centers.T[1], 'r+', label = 'RD centers')

plt.ylabel('Dec [deg]', fontsize=17)
plt.xlabel('RA [deg]', fontsize=17)
#plt.xlim(0.0, 0.8)
#plt.ylim(0.0, 0.8)
plt.legend(bbox_to_anchor=(1.05, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()


# ### ICL stacked profiles comparison 

# In[128]:


from astropy.table import Table
new_profile= Table.read(dir_ind+'jacknife_profile_%i.txt'%2, format='ascii')
new_profile.colnames


# In[129]:


fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')


yregionsprofiles = []
for i in range(40):
    new_profile = Table.read(dir_ind+'jacknife_profile_%i.txt'%i, format='ascii')
    x = new_profile['# xx'] *(0.263/60.)
    y = new_profile['r_mean'] /(0.263**2)      
    yregionsprofiles.append(y)


ymeanjacknife = np.array(yregionsprofiles)
means = [np.mean([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymeanjacknife.T]
std_  = [np.std([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymeanjacknife.T]
np.savetxt('moments_redmapper4.txt', list(zip(x, means, std_)), fmt='%f', header='# xx, means, std_') 

for i in range(40):
    new_profile = Table.read(dir_ind+'jacknife_profile_%i.txt'%i, format='ascii')
    #new_profilename = (((new_path.split('/')[-1]).split('.')[0]).replace('_', ' ')).replace('profile', 'HS')
    x = new_profile['# xx'] *(0.263/60.)
    y = new_profile['r_mean'] /(0.263**2)
    plt.plot(x, y, '-', lw=2,alpha=0.1,color='k',label='')

    
plt.plot(x, np.array(means), '-', lw=1.5,alpha=0.6,color='r',label='mean')
plt.plot(x, np.array(means)+np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.plot(x, np.array(means)-np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')


plt.annotate(str_ngals[ngal_bins-1], 
             xy=(0.6,0.95),xycoords='axes fraction',
             fontsize=18)
plt.annotate('N$_{cl}$ = '+str(Ncl), 
             xy=(0.6,0.9),xycoords='axes fraction',
             fontsize=18)
plt.ylabel('flux [arcsec$^{-2}$]', fontsize=17)
plt.xlabel('r [arcmin]', fontsize=17)
#plt.xlim(0.0, 0.8)
plt.ylim(-500, 10000.)
#plt.legend(bbox_to_anchor=(0.53, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()

