
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 

# In[2]:


import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys
from astropy.table import Table


# ### ICL jacknife profiles subttraction

# In[3]:


str_ngals = ['Ngals = [20;30)',
             'Ngals = [30;45)',
             'Ngals = [45;60)',
             r'Ngals = [60;$\infty$)']
moments = ['moments_redmapper1.txt',
             'moments_redmapper2.txt',
             'moments_redmapper3.txt',
             'moments_redmapper4.txt']
moments_sub = ['moments1_subtrac.txt',
             'moments2_subtrac.txt',
             'moments3_subtrac.txt',
             'moments4_subtrac.txt']


# In[ ]:


# computing all the moments_sub files (files with the subtracted moments)
for bin_nga in range(len(moments_sub)):    
    #jacknife_profile=np.loadtxt('moments_redmapper.txt')
    jacknife_profile=np.loadtxt(moments[bin_nga])
    jacknife_rnd=np.loadtxt('moments_rand.txt')
    r_red, m_red, std_red = jacknife_profile.T[0], jacknife_profile.T[1], jacknife_profile.T[2]
    r_rnd, m_rnd, std_rnd = jacknife_rnd.T[0], jacknife_rnd.T[1], jacknife_rnd.T[2]
    mea = m_red-m_rnd
    up = (m_red+std_red) - (m_rnd+std_rnd)
    low =  (m_red-std_red) - (m_rnd-std_rnd)
    np.savetxt(moments_sub[bin_nga], list(zip(r_red, mea, up, low)), fmt='%f', header='# xx, means, up, low') 


# In[58]:


ngal_bins=1

#jacknife_profile=np.loadtxt('moments_redmapper.txt')
jacknife_profile=np.loadtxt(moments[ngal_bins-1])
jacknife_rnd=np.loadtxt('moments_rand.txt')

r_red, m_red, std_red = jacknife_profile.T[0], jacknife_profile.T[1], jacknife_profile.T[2]
r_rnd, m_rnd, std_rnd = jacknife_rnd.T[0], jacknife_rnd.T[1], jacknife_rnd.T[2]

mea = m_red-m_rnd
up = (m_red+std_red) - (m_rnd+std_rnd)
low =  (m_red-std_red) - (m_rnd-std_rnd)
np.savetxt(moments_sub[ngal_bins-1], list(zip(r_red, mea, up, low)), fmt='%f', header='# xx, means, up, low') 


# In[5]:


from matplotlib import gridspec
fig = plt.figure(figsize=(8,8))  
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
ax0 = plt.subplot(gs[0])
plt.xscale('log')

plt.grid(alpha=0.4)

plt.plot(r_red, m_red, '-', lw=1.5,alpha=0.6,color='#1f77b4',label='RM mean')
plt.fill_between(r_red,m_red+std_red,m_red-std_red,color='#1f77b4', alpha=0.2, label='RM std')

plt.plot(r_rnd, m_rnd, '-', lw=1.5,alpha=0.6,color='#ff7f0e',label='RD mean')
plt.fill_between(r_red,m_rnd+std_rnd,m_rnd-std_rnd,color='#ff7f0e', alpha=0.2, label='RD std')


plt.annotate(str_ngals[ngal_bins-1], 
             xy=(0.6,0.55),xycoords='axes fraction',
             fontsize=18)

plt.ylim(-500, 8000)
plt.ylabel('flux [arcsec$^{-2}$]', fontsize=17)
plt.xlabel('r [arcmin]', fontsize=17)
plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax0.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
    
plt.legend(bbox_to_anchor=(0.6, 0.98), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)

ax1 = plt.subplot(gs[1],sharex=ax0)
plt.xscale('log')

plt.grid(alpha=0.4)

plt.plot(r_red, m_red, '-', lw=1.5,alpha=0.6,color='#1f77b4',label='')
plt.fill_between(r_red,m_red+std_red,m_red-std_red,color='#1f77b4', alpha=0.2, label='')

plt.plot(r_rnd, m_rnd, '-', lw=1.5,alpha=0.6,color='#ff7f0e',label='')
plt.fill_between(r_red,m_rnd+std_rnd,m_rnd-std_rnd,color='#ff7f0e', alpha=0.2, label='')

plt.ylabel('flux [arcsec$^{-2}$]', fontsize=14)
plt.xlabel('Radius [arcmin]', fontsize=17)
plt.ylim(-12, 12)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax1.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()
##############################################################################


# In[27]:


from matplotlib import gridspec
fig = plt.figure(figsize=(8,8))  
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
ax0 = plt.subplot(gs[0])
plt.xscale('log')

plt.grid(alpha=0.4)


mea = m_red-m_rnd
up = (m_red+std_red) - (m_rnd+std_rnd)
low =  (m_red-std_red) - (m_rnd-std_rnd)

plt.plot(r_red, mea, '-', lw=1.5,alpha=0.6,color='C4',label='RM - RD mean')
plt.fill_between(r_red, up, low, color='C4', alpha=0.2, label='RM - RD std')

plt.annotate(str_ngals[ngal_bins-1], 
             xy=(0.58,0.73),xycoords='axes fraction',
             fontsize=18)

plt.ylim(-500, 8000)
plt.ylabel('flux [arcsec$^{-2}$]', fontsize=17)
plt.xlabel('r [arcmin]', fontsize=17)
plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax0.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
    
plt.legend(bbox_to_anchor=(0.5, 0.98), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)

ax1 = plt.subplot(gs[1],sharex=ax0)
plt.xscale('log')

plt.grid(alpha=0.4)

plt.plot(r_red, mea, '-', lw=1.5,alpha=0.6,color='C4',label='')
plt.fill_between(r_red, up, low, color='C4', alpha=0.2, label='')

plt.ylabel('flux [arcsec$^{-2}$]', fontsize=14)
plt.xlabel('Radius [arcmin]', fontsize=17)
plt.ylim(-12, 16)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax1.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()
##############################################################################


# In[28]:


from matplotlib import gridspec
fig = plt.figure(figsize=(8,8))  
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
ax0 = plt.subplot(gs[0])
plt.xscale('log')

plt.grid(alpha=0.4)


colors = ['C1','C2','C3','C4']


for i in range(4):
    
    profile_sub=np.loadtxt(moments_sub[i])
    r_red, mea, up, low = profile_sub.T[0], profile_sub.T[1], profile_sub.T[2], profile_sub.T[3]
    
    plt.plot(r_red, mea, '-', lw=1.5,alpha=0.6,color=colors[i],label=str_ngals[i])
    plt.fill_between(r_red, up, low, color=colors[i], alpha=0.15, label='')

plt.ylabel('flux [arcsec$^{-2}$]', fontsize=17)
plt.xlabel('r [arcmin]', fontsize=17)
plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax0.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
    
plt.legend(bbox_to_anchor=(0.5, 0.98), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)

ax1 = plt.subplot(gs[1],sharex=ax0)
plt.xscale('log')

plt.grid(alpha=0.4)

colors = ['C1','C2','C3','C4']


for i in range(4):
    
    profile_sub=np.loadtxt(moments_sub[i])
    r_red, mea, up, low = profile_sub.T[0], profile_sub.T[1], profile_sub.T[2], profile_sub.T[3]
    
    plt.plot(r_red, mea, '-', lw=1.5,alpha=0.6,color=colors[i],label='')
    plt.fill_between(r_red, up, low, color=colors[i], alpha=0.15, label='')

plt.ylabel('flux [arcsec$^{-2}$]', fontsize=14)
plt.xlabel('Radius [arcmin]', fontsize=17)
plt.ylim(-4, 16)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
plt.locator_params(axis='y', nbins=5)
[ax1.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()
##############################################################################


# In[54]:


from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.3)
zph = 0.275
ang_d_z = cosmo.angular_diameter_distance(zph)
angd_converted_z = ang_d_z * cosmo.h
One_Mpc_in_degrees = (180. * 1.)/ (np.pi * angd_converted_z.value)
One_Mpc_in_arcmins = One_Mpc_in_degrees  * 60.
One_Mpc_in_arcsecs = One_Mpc_in_arcmins  * 60.
print('%.3f'%(One_Mpc_in_arcmins)+' arcmin correspond to 1 Mpc h^-1 in the z=0.275')
print('%.3f'%(One_Mpc_in_arcsecs)+' arcsec correspond to 1 Mpc h^-1 in the z=0.275')


# In[55]:


from matplotlib import gridspec
fig = plt.figure(figsize=(8,8))  
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
ax0 = plt.subplot(gs[0])
plt.xscale('log')

plt.grid(alpha=0.4)


colors = ['C1','C2','C3','C4']


for i in range(4):
    
    profile_sub=np.loadtxt(moments_sub[i])
    r_red, mea, up, low = profile_sub.T[0], profile_sub.T[1], profile_sub.T[2], profile_sub.T[3]
    
    r_red_mpc = r_red/One_Mpc_in_arcmins
    mea_mpc = mea/(One_Mpc_in_arcsecs**2)
    up_mpc = up/(One_Mpc_in_arcsecs**2)
    low_mpc = low/(One_Mpc_in_arcsecs**2)
    
    plt.plot(r_red_mpc, mea_mpc, '-', lw=1.5,alpha=0.6,color=colors[i],label=str_ngals[i])
    plt.fill_between(r_red_mpc, up_mpc, low_mpc, color=colors[i], alpha=0.15, label='')

plt.ylabel('flux [Mpc$^{-2}$ h$^{2}$]', fontsize=17)
plt.xlabel('radius [Mpc h$^{-1}$]', fontsize=17)
plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax0.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
    
plt.legend(bbox_to_anchor=(0.5, 0.98), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)

ax1 = plt.subplot(gs[1],sharex=ax0)
plt.xscale('log')

plt.grid(alpha=0.4)

colors = ['C1','C2','C3','C4']


for i in range(4):
    
    profile_sub=np.loadtxt(moments_sub[i])
    r_red, mea, up, low = profile_sub.T[0], profile_sub.T[1], profile_sub.T[2], profile_sub.T[3]
    
    r_red_mpc = r_red/One_Mpc_in_arcmins
    mea_mpc = mea/(One_Mpc_in_arcsecs**2)
    up_mpc = up/(One_Mpc_in_arcsecs**2)
    low_mpc = low/(One_Mpc_in_arcsecs**2)
    
    plt.plot(r_red_mpc, mea_mpc, '-', lw=1.5,alpha=0.6,color=colors[i],label='')
    plt.fill_between(r_red_mpc, up_mpc, low_mpc, color=colors[i], alpha=0.15, label='')

plt.ylabel('flux [Mpc$^{-2}$ h$^{2}$]', fontsize=14)
plt.xlabel('radius [Mpc h$^{-1}$]', fontsize=17)
plt.ylim(-0.0001, 0.0002)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
plt.locator_params(axis='y', nbins=5)
[ax1.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()
##############################################################################

