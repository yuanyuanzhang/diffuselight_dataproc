import numpy as np
import astropy.io.fits as pyfits
import os
import os.path
from numpy import cos
from astropy.wcs import WCS
import ConfigParser
import sys

ini_file=sys.argv[1]
ii_ind=int(sys.argv[2])

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
catalog=config.get('input', 'catalog_file')
save_dir='./'

img_dis=float(config.get('parameters', 'img_dis'))
cat_dis=float(config.get('parameters', 'cat_dis'))
npixels=int(config.get('parameters', 'pixel_dis'))*2
bands=[ii.strip() for ii in config.get('parameters', 'bands').split(',')]
ra_col=config.get('input', 'ra')
dec_col=config.get('input', 'dec')


cat=pyfits.open(catalog)[1].data
print ra_col, dec_col, ii_ind
for download_band in bands:
  ## Need to have swarp config file
  #temp_dir='temp_'+ini_file+'_ind%i_%i'%(loind, hiind)
  #os.system('mkdir '+temp_dir)
  #os.system('cp Y3A2_v2_swarp.config '+temp_dir+'/.')
  #os.chdir(temp_dir)
  coadd_images = 1
  if coadd_images:
    ra_ii=cat[ii_ind][ra_col]
    dec_ii=cat[ii_ind][dec_col]
    dir_ii=save_dir+'object_%i'%ii_ind
     
    file_out=dir_ii+'/image_list.csv'
    if os.path.isfile(file_out): 
      file_list=np.loadtxt(file_out, skiprows=1, usecols=(2, 4, 7, 8, 9), delimiter=',', dtype=[('band', np.str_, 1), ('mag_zeros', float), ('file', np.str_, 100), ('quality', np.str_, 10), ('flag', int)])
      bands=file_list['band']
      files=file_list['file']
      quality=file_list['quality']
      mag_zeros=file_list['mag_zeros']
      qual_flags=file_list['flag']

      ind_bands, =np.where( (bands == download_band) & (quality == 'True') & (qual_flags == 0))
      if len(ind_bands)>1:
       ff = open('mylist','w')
       ww = open('myweightlist', 'w')
       ll = open('fluxscalelist', 'w')
       for ind_jj in ind_bands:
           mag_zero=mag_zeros[ind_jj]
           file=files[ind_jj]
           file_name=file.split('.')[0]
           scale=10.0**(-0.4*(mag_zero-30.0))
        
           file_file=dir_ii+'/'+file_name+'.fits'
           file_img=file_file+'[0]'
           file_wht=file_file+'[2]'
           if os.path.isfile(file_file) & (scale < 1000): 
              ff.write(file_img+'\n')
              ww.write(file_wht+'\n')
              ll.write( '%f \n' %(scale) )

       ff.close()
       ww.close()
       ll.close()
       os.system('rm -f tempim_rand.fits')
       os.system('rm -f tempwt_rand.fits')
       swarp_cmd=' swarp  -c default.swarp  -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE 0.2630000 -PROJECTION_TYPE TAN -CENTER_TYPE MANUAL -CENTER %f,%f -IMAGE_SIZE %i,%i -SUBTRACT_BACK N  -BLANK_BADPIXELS Y  -COPY_KEYWORDS  PSF_FWHM,PSF_BETA,FWHMHOMO   -DELETE_TMPFILES Y -FSCALASTRO_TYPE VARIABLE  -FSCALE_DEFAULT @fluxscalelist  -FSCALE_KEYWORD nokey -RESAMPLE Y  -COMBINE Y  -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE  @myweightlist  -HEADER_ONLY N   -NTHREADS 8  @mylist -COMBINE_TYPE weighted  -WRITE_XML Y -XML_NAME swarp.xml -IMAGEOUT_NAME tempim_rand.fits  -WEIGHTOUT_NAME tempwt_rand.fits '%(ra_ii, dec_ii, npixels, npixels)
       if os.path.isfile(dir_ii+'/coadd_%i_'%ii_ind+download_band+'.fits') == 0:
          os.system(swarp_cmd)
             
       if os.path.isfile('tempim_rand.fits') & os.path.isfile('tempwt_rand.fits'):
        if os.path.getsize('tempim_rand.fits') > 300000 and os.path.getsize('tempwt_rand.fits') > 300000: 
          hdulist = pyfits.open('tempim_rand.fits')
          prihdr=hdulist[0].header
          newheader=prihdr
          img=hdulist[0].data
          hdulist_wt = pyfits.open('tempwt_rand.fits')
          wt=hdulist_wt[0].data

          new_hdu_img = pyfits.PrimaryHDU(img, header=newheader)
          new_hdu_wt =  pyfits.ImageHDU(wt)
          new_hdulist=pyfits.HDUList([new_hdu_img, new_hdu_wt])
          new_hdulist.writeto(dir_ii+'/coadd_%i_'%ii_ind+download_band+'.fits', clobber=True)



