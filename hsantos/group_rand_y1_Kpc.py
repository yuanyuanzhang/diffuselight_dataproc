
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 


get_ipython().magic('matplotlib inline')
import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.3)



ini_file= 'rand_y1.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir=config.get('output', 'save_dir')
grid_output_dir=config.get('grid_output', 'save_dir')
rand=pyfits.open(catalog)[1].data
flags=np.zeros(len(rand))+1
indices=np.arange(len(rand))

#bands=[ii.strip() for ii in config.get('parameters', 'bands').split(',')]
#start_index=int(config.get('parameters', 'start_index'))

for file_flag in flag_files:
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        print(sum(flag))
        if len(flag) == len(flags):
            flags=flags*flag
ind, =np.where( (flags > 0.5) & (flags<1.5) )
print(len(ind), len(indices))



####
second_path = 'fermigrid_rand/'
dat_type=[('distance', float), ('mean', float), ('median', float)]

to_be_removed = []

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    file_jj_g=input_dir+second_path+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)
    if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
        continue
    else:
        print('there is no profile for the cluster with index '+str(jj))
        to_be_removed.append(jj)

# remove the clusters indices that there is no profiles
for rem in to_be_removed:
    ind = np.delete(ind, list(ind).index(rem))
    
print('total number of clusters to use: '+str(len(ind)))
        



fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')


maxflux = 0
yregionsprofiles = []
for i in range(len(ind)):
    second_path = 'fermigrid_rand/'
    file_name=input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind[i],ind[i])    
    if os.path.isfile(file_name):        
        dat_type=[('distance', float), ('mean', float), ('median', float)]
        data_temp=np.loadtxt(file_name, dtype=dat_type)

        xxfactor=cosmo.kpc_proper_per_arcmin(0.275)*0.263/60
        yyfactor=1.0/(xxfactor**2)
        
        x = data_temp['distance'] *xxfactor
        y = data_temp['mean'] *yyfactor
        yregionsprofiles.append(y)   

        if max(y)>maxflux:
            maxflux = max(y)
            index_ = i   


#ind=np.delete(ind, index_)
ymean = np.array(yregionsprofiles)
means = [np.mean([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymean.T]


for i in range(len(ind)):
    second_path = 'fermigrid_rand/'
    file_name=input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind[i],ind[i])    
    if os.path.isfile(file_name):        
        dat_type=[('distance', float), ('mean', float), ('median', float)]
        data_temp=np.loadtxt(file_name, dtype=dat_type)
        xxfactor=cosmo.kpc_proper_per_arcmin(0.275)*0.263/60
        yyfactor=1.0/(xxfactor**2)
        
        x = data_temp['distance'] *xxfactor
        y = data_temp['mean'] *yyfactor

        plt.plot(x, y, '-', lw=2,alpha=0.1,color='k')


plt.plot(x, np.array(means), '-', lw=1.5,alpha=0.6,color='r',label='')
plt.ylabel('flux [Kpc$^{-2}$]', fontsize=17)
plt.xlabel('r [Kpc]', fontsize=17)
#plt.xlim(0.0, 0.8)
#plt.ylim(-6,-0.5)

plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
#plt.legend(bbox_to_anchor=(1.05, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.show()




fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')

plt.plot(x, np.array(means), '-', lw=1.5,alpha=0.6,color='r',label='mean')
#plt.plot(x, np.array(means)+np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
#plt.plot(x, np.array(means)-np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.ylabel('flux [Kpc$^{-2}$]', fontsize=17)
plt.xlabel('r [Kpc]', fontsize=17)
#plt.xlim(0.0, 0.8)
#plt.ylim(-6.2,-0.5)

plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
#plt.legend(bbox_to_anchor=(1.05, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.show()


####
second_path = 'fermigrid_rand/'
dat_type=[('distance', float), ('mean', float), ('median', float)]
data_temp=np.loadtxt(input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind[10],ind[10]), dtype=dat_type)
ncluster=len(ind)
nbins=len(data_temp['distance'])#20#399
yy_g=np.zeros([nbins, ncluster])+99999
yy_r=np.zeros([nbins, ncluster])+99999
yy_g_med=np.zeros([nbins, ncluster])+99999
yy_r_med=np.zeros([nbins, ncluster])+99999
xx=np.zeros([nbins, ncluster])+99999
#xx=data_temp['distance']
ct=0

for jji, jj in enumerate(ind):

    
    file_jj_g=input_dir+second_path+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)

    if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
        data_g=np.loadtxt(file_jj_g, dtype=dat_type)
        data_r=np.loadtxt(file_jj_r, dtype=dat_type)

        xxfactor=cosmo.kpc_proper_per_arcmin(0.275)*0.263/60
        yyfactor=1.0/(xxfactor**2)

        if len(data_g['distance'])==nbins and len(data_r['distance'])==nbins:
            
            yy_g[:, jji]=data_g['mean']*yyfactor
            yy_g_med[:, jji]=data_g['median']*yyfactor
            yy_r[:, jji]=data_r['mean']*yyfactor
            yy_r_med[:, jji]=data_r['median']*yyfactor
            xx[:, jji]=data_temp['distance']*xxfactor
            ct=ct+1

print('total number of points to use:', ct)




xmaxlist = []
for i in range(len(xx)):
    xmaxlist.append(max(xx[i]))
print(max(xmaxlist))


xxbins = np.arange(0.0,2000.1, 2000/nbins )
phy_xx = (xxbins[:-1]+xxbins[1:])/2
#yyrbins = np.arange(-29e-5,7300e-5,(7300e-5--29e-5)/len(xxbins) )

phy_yy_g=list(ind)
phy_yy_g_med=list(ind)
phy_yy_r=list(ind)
phy_yy_r_med=list(ind)

for ix in range(len(xx.T)): # ii is each cluster 
    phy_flux_g = []
    phy_flux_med_g = []
    phy_flux_r = []
    phy_flux_med_r = []
    
    for xxb in range(len(xxbins)-1):
        xxmask = (xx.T[ix]>=xxbins[xxb]) * (xx.T[ix]<xxbins[xxb+1])
        phy_flux_g.append(np.mean(yy_g.T[ix][xxmask]))
        phy_flux_med_g.append(np.mean(yy_g_med.T[ix][xxmask]))
        phy_flux_r.append(np.mean(yy_r.T[ix][xxmask]))
        phy_flux_med_r.append(np.mean(yy_r_med.T[ix][xxmask]))
        
    phy_yy_g[ix]=phy_flux_g
    phy_yy_g_med[ix]=phy_flux_med_g
    phy_yy_r[ix]=phy_flux_r
    phy_yy_r_med[ix]=phy_flux_med_r
    


dir_ind=save_dir+'rand_y1_jacknife_rand/'
os.system('mkdir '+dir_ind)

yygmean_ind=np.zeros(nbins)
yygmed_ind=np.zeros(nbins)
yyrmean_ind=np.zeros(nbins)
yyrmed_ind=np.zeros(nbins)

for jj in range(nbins): # binning in radius

    temp_g_mean=np.array(phy_yy_g).T[jj]
    temp_g_med=np.array(phy_yy_g_med).T[jj]
    temp_r_mean=np.array(phy_yy_r).T[jj]
    temp_r_med=np.array(phy_yy_r_med).T[jj]

    ind_temp, =np.where((temp_g_mean < 30000) & (temp_g_mean >-30000) &                         (temp_g_med < 30000) & (temp_g_med >-30000) &                         (temp_r_mean < 30000) & (temp_r_mean >-30000) &                         (temp_r_med < 30000) & (temp_r_med >-30000))

    arr=temp_g_mean[ind_temp]
    yygmean_ind[jj]=np.mean(arr)

    arr=temp_g_med[ind_temp]
    yygmed_ind[jj]=np.mean(arr)

    arr=temp_r_mean[ind_temp]
    yyrmean_ind[jj]=np.mean(arr)

    arr=temp_r_med[ind_temp]
    yyrmed_ind[jj]=np.mean(arr)

z=list(zip(phy_xx, yygmean_ind,yygmed_ind,yyrmean_ind,yyrmed_ind)) 
np.savetxt(dir_ind+'kpc_jacknife_profile.txt', z, fmt='%f, %f, %f, %f, %f', header='# xx, g_mean, g_median, r_mean, r_median')


# ### ICL stacked profiles comparison 



from astropy.table import Table
new_profile= Table.read(dir_ind+'kpc_jacknife_profile.txt', format='ascii')
new_profile.colnames


fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')



new_profile = Table.read(dir_ind+'kpc_jacknife_profile.txt', format='ascii')
x = new_profile['# xx']
y = new_profile['r_mean']       

zeromask=(np.array(y)!=np.nan)
plt.plot(x[zeromask], y[zeromask], '-', lw=1.5,alpha=0.5,color='r',label='mean')
plt.ylabel('flux [Kpc$^{-2}$]', fontsize=17)
plt.xlabel('r [Kpc]', fontsize=17)
#plt.xlim(0.0, 0.8)
#plt.ylim(0.0, 0.8)


plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.savefig(dir_ind+'Jackprof.png', format='png', dpi=100)
#plt.legend(bbox_to_anchor=(1.05, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.show()

