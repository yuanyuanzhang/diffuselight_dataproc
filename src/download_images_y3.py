import math
import numpy as np
import astropy.io.fits as pyfits
import os
import os.path
from numpy import cos
import easyaccess as ea
import ConfigParser
import sys
from os.path import expanduser
home = expanduser("~")


###########change usr, pwd to be read from desservices.ini
############ do i want to use only the accepted images?

ini_file_services=home+"/.desservices.ini"
config_serv = ConfigParser.ConfigParser()
inifile_serv = config_serv.read(ini_file_services)
desdm_usr=config_serv.get('db-desoper', 'user')#raw_input("Enter DESDM account: ")
desdm_pwd=config_serv.get('db-desoper', 'passwd')#raw_input('Enter DESDM password: ')
main_http_site='https://desar.cosmology.illinois.edu/DESFiles/desarchive/'

ini_file=sys.argv[1]
ii_ind=int(sys.argv[2])

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
save_dir='./'
list_bands=[jj.strip() for jj in config.get('parameters', 'bands').split(',')]

download_images=1
if download_images:
    dir_ii=save_dir + 'object_%i'%ii_ind   
    image_list=dir_ii+'/image_list.csv'
    if os.path.isfile(image_list):
        file_list=np.loadtxt(image_list, skiprows=1, usecols=(2, 3, 7), delimiter=',', dtype=[('band', np.str_, 1), ('paths', np.str_, 100), ('file_name', np.str_, 100),])
        bands=file_list['band']
        paths=file_list['paths']
        file_names=file_list['file_name']

        for download_band in list_bands:
            ind_bands, =np.where( bands == download_band)
            if len(ind_bands) > 1:
               for ind_jj in ind_bands:
                   path_jj=(np.asarray(paths))[ind_jj]
                   filename_jj=np.asarray(file_names)[ind_jj]
                   link_img=main_http_site+path_jj+'/'+filename_jj+'.fz'
                   #link_img=path_jj+'/'+filename_jj+'.fz'
                   wget_command_img="wget --http-user="+desdm_usr+"  --http-password="+desdm_pwd+' '+link_img+" -P "+dir_ii
                   #wget_command_img="rsync --password-file=/home/s1/ynzhang/.des_rsync_pass erykoff@$DESREMOTE_RSYNC/"+link_img+"  "+band_dir_ii
                   #print wget_command_img
                   file_img=dir_ii+'/'+filename_jj
              
                   if os.path.isfile(file_img)==0:
                      if os.path.isfile(file_img+'.fz')==1:
                         os.system('rm -f '+file_img + '.fz')
                      os.system(wget_command_img)
                   if os.path.isfile(file_img+'.fz')==1:
                      os.system('funpack -D '+file_img + '.fz')
                   if os.path.isfile(file_img)==1:
                      file_image=file_img+'[0]'
                      file_mask=file_img+'[1]'
                      file_wht=file_img+'[2]'
                      
                      # convolving weights with mask so that masks are considered when stacking
                      hdulist = pyfits.open(file_img)
                      prihdr=hdulist[0].header
                      img=hdulist[0].data
                      mask=hdulist[1].data
                      wt=hdulist[2].data

                      ind_mask=np.where(mask > 0)
                      wt[ind_mask]=0
                      new_hdu_img = pyfits.PrimaryHDU(img, header=prihdr)
                      new_hdu_msk = pyfits.ImageHDU(mask)
                      new_hdu_wt =  pyfits.ImageHDU(wt)
                      new_hdulist=pyfits.HDUList([new_hdu_img, new_hdu_msk, new_hdu_wt])
                      new_hdulist.writeto(file_img, clobber=True) ### Test this line##########
                   else:
                      print 'not successful in downloading image '+filename_jj
                      os.system('rm -f '+file_img)
      
