
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
import astropy.cosmology
import os
import os.path
import configparser
import sys
from astropy.cosmology import FlatwCDM
cosmo = FlatwCDM(H0=70, Om0=0.3)

ini_file= 'redmapper_y1.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir=config.get('output', 'save_dir')
grid_output_dir=config.get('grid_output', 'save_dir')
rdmp=pyfits.open(catalog)[1].data
flags=np.zeros(len(rdmp))+1
indices=np.arange(len(rdmp))
dir_rdmp=save_dir+'redmapper_y1_jacknife_profiles/'
dir_rand=save_dir+'rand_y1_jacknife_rand/'
#yzhang_randprofiles = save_dir+'rand_y3a2_6422v2/unmasked_jacknife_profiles_foregroundflag/'
yzhang_randprofiles = save_dir+'rand_y3a2_6422v2/unmasked_jacknife_profiles_foregroundflag/'
yzhang_z020_035 = save_dir+'redmapper_y3a2_6422v2/jacknife_profiles_foregroundflag_z020_035/'
yzhang_z035_050 = save_dir+'redmapper_y3a2_6422v2/jacknife_profiles_foregroundflag_z035_050/'
yzhang_z050_065 = save_dir+'redmapper_y3a2_6422v2/jacknife_profiles_foregroundflag_z050_065/'


def jacknife_statistics(dat1, dat2 = 0):
    
    njack, narray = dat1.shape
    if dat1.shape != dat2.shape:
        dat2 = 0 #np.zeros([narray, jack]) # alternatively dat2 is 0
        print('no second array, using 0')
    mean = np.zeros(narray); err = np.zeros(narray)
    err_factor = np.sqrt(njack-1)

    for ii in range(narray):
        bkg = 0 #dat1[:, narray-1] - dat2[:, narray-1] # 0 for default
        mean[ii] = np.mean(dat1[:, ii] - dat2[:, ii]-bkg)
        err[ii] = np.std(dat1[:, ii] - dat2[:, ii] - bkg) * err_factor
    return mean, err



rand_array=pyfits.open(yzhang_randprofiles+'jacknife_profile_i.fits')[1].data
print(rand_array.columns.names)
xx=rand_array['x_axis'][0, :]



rdmp_array=pyfits.open(yzhang_z020_035+'jacknife_profile_i.fits')[1].data
#rdmp_array=pyfits.open('/data/des40.a/data/ynzhang/ICL_y3_redshift/redmapper_y3a2_6422v2/unmasked_jacknife_profiles_foregroundflag_z020_035/jacknife_profile_i.fits')[1].data
mean, err = jacknife_statistics(rdmp_array['mean'], rand_array['mean'])
xxfactor = 0.263*cosmo.kpc_proper_per_arcmin(0.275).value/60
yyfactor=1.0/xxfactor**2
lm0=5.0*np.log10(cosmo.luminosity_distance(0.275).value ) + 25.0
print(lm0)
plt.fill_between(xx*xxfactor, (mean-err)*yyfactor, (mean+err)*yyfactor, facecolor='r', alpha=0.25)
plt.plot(xx*xxfactor, mean*yyfactor, 'r', label='redshift 0.2 to 0.35')


rdmp_array=pyfits.open(yzhang_z035_050+'jacknife_profile_i.fits')[1].data
#rdmp_array=pyfits.open('/data/des40.a/data/ynzhang/ICL_y3_redshift/redmapper_y3a2_6422v2/unmasked_jacknife_profiles_foregroundflag_z035_050/jacknife_profile_i.fits')[1].data
mean, err = jacknife_statistics(rdmp_array['mean'], rand_array['mean'])
xxfactor = 0.263*cosmo.kpc_proper_per_arcmin(0.425).value/60
lm1=5.0*np.log10(cosmo.luminosity_distance(0.425).value ) + 25.0
yyfactor=1.0/xxfactor**2 * 10.0**( (lm1 - lm0) /2.5)
print(lm1)
plt.fill_between(xx*xxfactor, (mean-err)*yyfactor, (mean+err)*yyfactor, facecolor='k', alpha=0.25)
plt.plot(xx*xxfactor, mean*yyfactor, 'k', label='redshift 0.35 to 0.5')



rdmp_array=pyfits.open(yzhang_z050_065+'jacknife_profile_i.fits')[1].data
#rdmp_array=pyfits.open('/data/des40.a/data/ynzhang/ICL_y3_redshift/redmapper_y3a2_6422v2/unmasked_jacknife_profiles_foregroundflag_z050_065/jacknife_profile_i.fits')[1].data
mean, err = jacknife_statistics(rdmp_array['mean'], rand_array['mean'])
xxfactor = 0.263*cosmo.kpc_proper_per_arcmin(0.575).value/60
lm2=5.0*np.log10(cosmo.luminosity_distance(0.575).value ) + 25.0
yyfactor=1.0/xxfactor**2 * 10.0**( (lm2 - lm0) /2.5)
print(lm2)
plt.fill_between(xx*xxfactor, (mean-err)*yyfactor, (mean+err)*yyfactor, facecolor='b', alpha=0.25)
plt.plot(xx*xxfactor, mean*yyfactor, 'b', label='redshift 0.5 to 0.65')


plt.ylim([0.001, 600])
plt.xlim([1, 4000])
plt.legend(loc = 1)
plt.xlabel('r [kpc], proper distance')
plt.ylabel(r'Flux(r) [/kpc$^2$], mag_zeropoint = 30.0')
plt.yscale('log')
plt.xscale('log')
plt.savefig(save_dir+'redmapper_y3a2_6422v2/'+'temp.png')
plt.show()




