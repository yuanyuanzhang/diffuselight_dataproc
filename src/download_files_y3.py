import math
import numpy as np
import astropy.io.fits as pyfits
import os
import easyaccess as ea
import os.path
from numpy import cos
import ConfigParser
import sys

ini_file=sys.argv[1]
dbh =ea.connect()
cursor = dbh.cursor()

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
catalog=config.get('input', 'catalog_file')
save_dir='./'

img_dis=float(config.get('parameters', 'img_dis'))
cat_dis=float(config.get('parameters', 'cat_dis'))
bands=[jj.strip() for jj in config.get('parameters', 'bands').split(',')]
ra_col=config.get('input', 'ra')
dec_col=config.get('input', 'dec')

ii_ind=int(sys.argv[2])
cat=pyfits.open(catalog)[1].data
print ra_col, dec_col, ii_ind
download_files=1
if download_files:
    ra_ii=cat[ii_ind][ra_col]
    dec_ii=cat[ii_ind][dec_col]
    dis_selection=img_dis
    ra_lo=ra_ii-dis_selection/cos(dec_ii/180.0*math.pi)
    ra_hi=ra_ii+dis_selection/cos(dec_ii/180.0*math.pi)
    dec_lo=dec_ii-dis_selection
    dec_hi=dec_ii+dis_selection

    dir_ii=save_dir + 'object_%i'%ii_ind
    if not os.path.exists(dir_ii):
       os.system('mkdir '+dir_ii)
    
    # sql image names, quality assessment, zeropoint etc.
    file_out=dir_ii+'/file_list.csv'
    sql_command="select i.filename, i.filetype, i.band, i.pfw_attempt_id, i.expnum, i.ccdnum from Y3A1_image i, Y3A1_proctag t where t.tag='Y3A1_FINALCUT' and i.filetype='red_immask' and i.racmin < %f and i.racmax > %f and i.deccmin < %f and i.deccmax > %f and t.pfw_attempt_id = i.pfw_attempt_id "% (ra_hi, ra_lo, dec_hi, dec_lo)   
    #print sql_command 
    if os.path.isfile(file_out)== 1 :
       print 'temporary file list existed for object %i. Deleted.'%ii_ind    
       os.system('rm -f '+file_out)
    if os.path.isfile(file_out)== 0 :
       dbh.query_and_save(sql_command, file_out)

    if os.path.isfile(file_out):    
       file_list=np.loadtxt(file_out, skiprows=1, usecols=(0, 2, 3, 4, 5), delimiter=',', dtype=[('filename', np.str_, 200), ('band', np.str_, 5), ('pfwid', int), ('expnum', int), ('ccdnum', int)])
       file_names=file_list['filename']
       pfwid=file_list['pfwid']
       expnum= file_list['expnum']
       f = open(dir_ii+'/image_list.csv', 'w') # organize image list
       f.write('#filename, pfw_attempt_id,  band, path, mag_zero, pfw_attempt_id, ccd, filename, accepted, flag, expnum \n')
       for jj in range(len(file_names)):
           file_qa_out=dir_ii+'/'+file_names[jj]+'.zeropoint+QA.csv'
           sql_command= "select z.mag_zero, z.expnum, z.ccdnum, z.tag, z.band, f.path, ev.accepted, qa.flag, qa.expnum,  ev.expnum from prod.zeropoint@DESOPER z, prod.file_archive_info@DESOPER f, PROD.FINALCUT_EVAL@DESOPER ev, PROD.QA_SUMMARY@DESOPER qa where z.version='v2.0' and z.imagename = '%s' and f.filename = '%s' and qa.pfw_attempt_id= %i and ev.pfw_attempt_id = %i "%(file_names[jj], file_names[jj], pfwid[jj], pfwid[jj])
           #print sql_command
           if os.path.isfile(file_qa_out)== 0 :
              dbh.query_and_save(sql_command, file_qa_out)
           #organize image list
           if os.path.isfile(file_qa_out):
             files=np.loadtxt(file_qa_out, skiprows=1, delimiter=',', usecols=(0, 5, 6, 7, 8, 9), dtype=[('mag_zero', float), ('path', np.str_, 200), ('accepted', np.str_, 20), ('flag', int), ('expnum', int), ('expnum2', int)])
             if np.array([files['mag_zero']]).size == 1:
                f.write(file_names[jj]+',')
                f.write('%i,'%pfwid[jj])
                f.write(file_list['band'][jj]+',')
                f.write('%s,'%files['path'])
                f.write('%f,'%files['mag_zero'])
                f.write('%i,'%pfwid[jj])
                f.write('%i,'%file_list['ccdnum'][jj])
                f.write(file_names[jj]+',')
                f.write('%s,'%files['accepted'])
                f.write('%s,'%files['flag'])
                f.write('%i'%file_list['expnum'][jj])
                f.write('\n')
             else:
                print 'error did not successfully load zeropint, quality assessment'
                print 'check '+file_names[jj]+' in '+file_out
           else:
                print 'error did not successfully Download files on zeropint, quality assessment'
                print 'check '+file_names[jj]+' in '+file_out
       f.close()
    else:
       print 'error did not successfully find any single exposure images, check ra dec %f %f'%(ra_ii, dec_ii)    
      
    #sql the coadd catalog
    dis_cat_selection=cat_dis
    ra_cat_lo=ra_ii-dis_cat_selection/cos(dec_ii/180.0*math.pi)
    ra_cat_hi=ra_ii+dis_cat_selection/cos(dec_ii/180.0*math.pi)
    dec_cat_lo=dec_ii-dis_cat_selection
    dec_cat_hi=dec_ii+dis_cat_selection
    sql_cmd="select o.ra, o.dec, o.a_image, o.b_image, o.kron_radius, o.theta_j2000, o.mag_auto_g, o.mag_auto_r, o.mag_auto_i, o.mag_auto_z, o.magerr_auto_i, o.magerr_auto_z, o.class_star_i, o.spread_model_i, o.spreaderr_model_i  from Y3A2_COADD_OBJECT_SUMMARY o where o.ra > %f and o.ra < %f and o.dec >%f and o.dec < %f "%(ra_cat_lo, ra_cat_hi, dec_cat_lo, dec_cat_hi)
    cat_out=dir_ii+'/coadd_catalog.csv'
    if os.path.isfile(cat_out)== 1 :
       print 'coadd object existed for object %i. Deleted.'%ii_ind 
       os.system('rm -f '+cat_out)
    if os.path.isfile(cat_out)== 0 :
       dbh.query_and_save(sql_cmd, cat_out) 
    if os.path.isfile(cat_out)== 0 :
       print 'error did not successfully download any coadd objects, check ra dec %f %f'%(ra_ii, dec_ii)    




