
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 



import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.3)



ini_file= 'redmapper_y1.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir=config.get('output', 'save_dir')
grid_output_dir=config.get('grid_output', 'save_dir')
rdmp=pyfits.open(catalog)[1].data
flags=np.zeros(len(rdmp))+1
indices=np.arange(len(rdmp))


str_ngals = ['$\lambda$ = [20;30)',
             '$\lambda$ = [30;45)',
             '$\lambda$ = [45;60)',
             r'$\lambda$ = [60;$\infty$)']
richness_dir = ['richness_20_30/',
                'richness_30_45/',
                'richness_45_60/',
                'richness_60_beyond/']


# getting indices in the flag files
for file_flag in flag_files:
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        if len(flag) == len(flags):
            flags=flags*flag
#ind, =np.where( (flags > 0.5) & (flags<1.5) )#& (rdmp['lambda_chisq']>= 40) & (rdmp['lambda_chisq'] < 40000000))

ind1, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 20) & (rdmp['lambda_chisq'] < 30))
print(len(ind1), len(indices))
ind2, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 30) & (rdmp['lambda_chisq'] < 45))
print(len(ind2), len(indices))
ind3, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 45) & (rdmp['lambda_chisq'] < 60))
print(len(ind3), len(indices))
ind4, =np.where( (flags > 0.5) & (flags<1.5) & (rdmp['lambda_chisq']>= 60) & (rdmp['lambda_chisq'] < 40000000))
print(len(ind4), len(indices))



inds = [ind1,ind2,ind3,ind4]
for ij in range(4):
    ngal_bins=ij
    ind = inds[ngal_bins-1]
    print('z = '+str(np.mean(rdmp['Z'][ind])))



ngal_bins=2
inds = [ind1,ind2,ind3,ind4]
ind = inds[ngal_bins-1]
Ncl = len(ind)

####
second_path = 'fermigrid_profiles/'
dat_type=[('distance', float), ('mean', float), ('median', float)]

to_be_removed = []

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    file_jj_g=input_dir+second_path+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)
    if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
        continue
    else:
        print('there is no profile for the cluster  with index '+str(jj))
        to_be_removed.append(jj)

# remove the clusters indices that there is no profiles
for rem in to_be_removed:
    ind = np.delete(ind, list(ind).index(rem))
    
print('total number of clusters to use: '+str(len(ind)))
        



####
second_path = 'fermigrid_profiles/'
dat_type=[('distance', float), ('mean', float), ('median', float)]
data_temp=np.loadtxt(input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind[10],ind[10]), dtype=dat_type)
ncluster=len(ind)
nbins=len(data_temp['distance'])#20#399
yy_g=np.zeros([nbins, ncluster])+99999
yy_r=np.zeros([nbins, ncluster])+99999
yy_g_med=np.zeros([nbins, ncluster])+99999
yy_r_med=np.zeros([nbins, ncluster])+99999
xx=np.zeros([nbins, ncluster])+99999
#xx=data_temp['distance']
ct=0

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    file_jj_g=input_dir+second_path+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)
    if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
        data_g=np.loadtxt(file_jj_g, dtype=dat_type)
        data_r=np.loadtxt(file_jj_r, dtype=dat_type)

        xxfactor=cosmo.kpc_proper_per_arcmin(0.275)*0.263/60
        yyfactor=1.0/(xxfactor**2)
        
        if len(data_g['distance'])==nbins and len(data_r['distance'])==nbins: # nbins: length of the profile, number of radius bins
            if 1:
                yy_g[:, jji]=data_g['mean']*yyfactor
                yy_g_med[:, jji]=data_g['median']*yyfactor
                yy_r[:, jji]=data_r['mean']*yyfactor
                yy_r_med[:, jji]=data_r['median']*yyfactor
                xx[:, jji]=data_temp['distance']*xxfactor
                ct=ct+1
        else:
            print(jji)


print('total number of clusters to use: '+str(ct))
        



xxbins = np.arange(0.0,2000.1, 2000/nbins )
phy_xx = (xxbins[:-1]+xxbins[1:])/2
#yyrbins = np.arange(-29e-5,7300e-5,(7300e-5--29e-5)/len(xxbins) )

phy_yy_g=list(ind)
phy_yy_g_med=list(ind)
phy_yy_r=list(ind)
phy_yy_r_med=list(ind)

for ix in range(len(xx.T)): # ii is each cluster 
    phy_flux_g = []
    phy_flux_med_g = []
    phy_flux_r = []
    phy_flux_med_r = []
    
    for xxb in range(len(xxbins)-1):
        xxmask = (xx.T[ix]>=xxbins[xxb]) * (xx.T[ix]<xxbins[xxb+1])
        phy_flux_g.append(np.mean(yy_g.T[ix][xxmask]))
        phy_flux_med_g.append(np.mean(yy_g_med.T[ix][xxmask]))
        phy_flux_r.append(np.mean(yy_r.T[ix][xxmask]))
        phy_flux_med_r.append(np.mean(yy_r_med.T[ix][xxmask]))
        
    phy_yy_g[ix]=phy_flux_g
    phy_yy_g_med[ix]=phy_flux_med_g
    phy_yy_r[ix]=phy_flux_r
    phy_yy_r_med[ix]=phy_flux_med_r
    
    


dir_rand=save_dir+'rand_y1_jacknife_rand/'
rand_profile= Table.read(dir_rand+'kpc_jacknife_profile.txt', format='ascii')
x_rand = rand_profile['# xx']
y_rand = rand_profile['r_mean']   



dir_ind=save_dir+'redmapper_y1_jacknife_profiles/'+richness_dir[ngal_bins-1]
os.system('mkdir '+dir_ind)

yygmean_ind=np.zeros(nbins)
yygmed_ind=np.zeros(nbins)
yyrmean_ind=np.zeros(nbins)
yyrmed_ind=np.zeros(nbins)

for jj in range(nbins): # binning in radius

    temp_g_mean=np.array(phy_yy_g).T[jj]-y_rand[jj]
    temp_g_med=np.array(phy_yy_g_med).T[jj]-y_rand[jj]
    temp_r_mean=np.array(phy_yy_r).T[jj]-y_rand[jj]
    temp_r_med=np.array(phy_yy_r_med).T[jj]-y_rand[jj]

    ind_temp, =np.where((temp_g_mean < 30000) & (temp_g_mean >-30000) &                         (temp_g_med < 30000) & (temp_g_med >-30000) &                         (temp_r_mean < 30000) & (temp_r_mean >-30000) &                         (temp_r_med < 30000) & (temp_r_med >-30000))
    
    arr=temp_g_mean[ind_temp]
    yygmean_ind[jj]=np.mean(arr)

    arr=temp_g_med[ind_temp]
    yygmed_ind[jj]=np.mean(arr)

    arr=temp_r_mean[ind_temp]
    yyrmean_ind[jj]=np.mean(arr)

    arr=temp_r_med[ind_temp]
    yyrmed_ind[jj]=np.mean(arr)

z=list(zip(phy_xx, yygmean_ind,yygmed_ind,yyrmean_ind,yyrmed_ind)) 
np.savetxt(dir_ind+'kpc_jacknife_profile.txt', z, fmt='%f, %f, %f, %f, %f', header='# xx, g_mean, g_median, r_mean, r_median')



## jacknife sampling

# getting the center position of rdmp clusters
coord_rdmp=np.transpose( np.array([rdmp[ind]['ra'], rdmp[ind]['dec']]) )
ncen = 40

if os.path.isfile('phy_centers.txt') == 0: #this is false, so the code is jumping this part
    km = kmeans_sample(coord_rdmp, ncen, maxiter=100, tol=1.0e-5)
    print("found centers:",km.centers)
    print("converged?",km.converged)
    np.savetxt('phy_centers.txt', km.centers)
centers=np.loadtxt('phy_centers.txt')

# find the rdmp cluster closest to the center of the region:
labels_rdmp=kmeans_radec.find_nearest(coord_rdmp, centers)
print(labels_rdmp)
print(str(len(labels_rdmp))+' clusters')
print('  ')

from astropy.table import Table
dir_rand=save_dir+'rand_y1_jacknife_rand/'
rand_profile= Table.read(dir_rand+'kpc_jacknife_profile.txt', format='ascii')
x_rand = rand_profile['# xx']
y_rand = rand_profile['r_mean']   


len_kmeans=centers.shape[0] # 40, number of regions
dir_ind=save_dir+'redmapper_y1_jacknife_profiles/'+richness_dir[ngal_bins-1]
os.system('mkdir '+dir_ind)

for ii in range(len_kmeans): # binning in kmean regions
    
    # subtracting clusters that belong to this region
    ind_rdmp,=np.where( (labels_rdmp != ii) )
    
    print('region %i:'%(ii))
    print(str(len(labels_rdmp)-len(ind_rdmp))+' cluster(s) were removed')
    print(str(len(ind_rdmp))+' cluster(s) were used')
    print('')
    
    yygmean_ind=np.zeros(nbins)
    yygmed_ind=np.zeros(nbins)
    yyrmean_ind=np.zeros(nbins)
    yyrmed_ind=np.zeros(nbins)
    
    for jj in range(nbins): # binning in radius
        
        temp_g_mean=np.array(phy_yy_g).T[jj]-y_rand[jj]
        temp_g_med=np.array(phy_yy_g_med).T[jj]-y_rand[jj]
        temp_r_mean=np.array(phy_yy_r).T[jj]-y_rand[jj]
        temp_r_med=np.array(phy_yy_r_med).T[jj]-y_rand[jj]
        
        ind_temp, =np.where((temp_g_mean < 30000) & (temp_g_mean >-30000) &                             (temp_g_med < 30000) & (temp_g_med >-30000) &                             (temp_r_mean < 30000) & (temp_r_mean >-30000) &                             (temp_r_med < 30000) & (temp_r_med >-30000) &                             (labels_rdmp != ii) )
        
        arr=temp_g_mean[ind_temp]
        yygmean_ind[jj]=np.mean(arr)
        
        arr=temp_g_med[ind_temp]
        yygmed_ind[jj]=np.mean(arr)
        
        arr=temp_r_mean[ind_temp]
        yyrmean_ind[jj]=np.mean(arr)
        
        arr=temp_r_med[ind_temp]
        yyrmed_ind[jj]=np.mean(arr)

    z=list(zip(phy_xx, yygmean_ind,yygmed_ind,yyrmean_ind,yyrmed_ind)) 
    np.savetxt(dir_ind+'kpc_jacknife_profile_%i.txt'%ii, z, fmt='%f,%f,%f,%f,%f', header='# xx, g_mean, g_median, r_mean, r_median')



# ### ICL stacked profiles comparison 


from astropy.table import Table
new_profile= Table.read(dir_ind+'jacknife_profile_%i.txt'%2, format='ascii')
new_profile.colnames


fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')


maxflux = 0
yregionsprofiles = []
for i in range(40):
    new_profile = Table.read(dir_ind+'kpc_jacknife_profile_%i.txt'%i, format='ascii')
    x = new_profile['# xx']
    y = new_profile['r_mean']   
    yregionsprofiles.append(y)   
    if max(y)>maxflux:
        maxflux = max(y)
        index_ = i   


#del yregionsprofiles[index_]
ymeanjacknife = np.array(yregionsprofiles)
means = [np.mean([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymeanjacknife.T]
std0 = [np.std([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymeanjacknife.T]
std_ = np.array(std0)*np.sqrt(len(ymeanjacknife)-1)
np.savetxt(dir_ind+'kpc_moments_rand.txt', np.array([x, means, std_]).T, fmt='%f', header='# # xx, means, std_')   

for i in range(40):   

    new_profile = Table.read(dir_ind+'kpc_jacknife_profile_%i.txt'%i, format='ascii')
    x = new_profile['# xx'] 
    y = new_profile['r_mean']  
    
    plt.plot(x, y, '-', lw=2,alpha=0.1,color='k',label='region %s'%(i))
    

plt.plot(x, np.array(means), '-', lw=1.5,alpha=0.6,color='r',label='mean')
plt.plot(x, np.array(means)+np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.plot(x, np.array(means)-np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.ylabel('flux [Kpc$^{-2}$]', fontsize=17)
plt.xlabel('r [Kpc]', fontsize=17)
#plt.xlim(0.0, 0.8)
#plt.ylim(0.0, 0.8)

plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.savefig(dir_ind+'Jackprof.png', format='png', dpi=100)
#plt.legend(bbox_to_anchor=(1.05, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.show()


fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')
plt.grid()


for i in range(40):   

    new_profile = Table.read(dir_ind+'kpc_jacknife_profile_%i.txt'%i, format='ascii')
    x = new_profile['# xx'] 
    y = new_profile['r_mean']  
    
    plt.plot(x, y, '-', lw=2,alpha=0.1,color='k',label='region %s'%(i))
    

plt.plot(x, np.array(means), '-', lw=1.5,alpha=0.6,color='r',label='mean')
plt.plot(x, np.array(means)+np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.plot(x, np.array(means)-np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.ylabel('flux [Kpc$^{-2}$]', fontsize=17)
plt.xlabel('r [Kpc]', fontsize=17)

plt.ylim(-0.2, 0.3)

plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
plt.savefig(dir_ind+'Jackprof.png', format='png', dpi=100)
plt.show()

