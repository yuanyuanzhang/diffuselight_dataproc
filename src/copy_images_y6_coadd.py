import math
import numpy as np
import astropy.io.fits as pyfits
import os
import os.path
from numpy import cos
import ConfigParser
import sys
from os.path import expanduser
import time
home = expanduser("~")
t0=time.time()

ini_file=sys.argv[1]
ii_ind=int(sys.argv[2])

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
save_dir='./'
list_bands=[jj.strip() for jj in config.get('parameters', 'bands').split(',')]

grid_input_images = config.get('grid_input', 'images_input_dir')

if 1:
    print "running download images"
    dir_ii=save_dir + 'object_%i'%ii_ind   
    image_list=dir_ii+'/image_list.csv'
    if os.path.isfile(image_list):
        file_list=np.loadtxt(image_list, skiprows=1, usecols=(2, 3, 0), delimiter=',', dtype=[('band', np.str_, 1), ('paths', np.str_, 100), ('file_name', np.str_, 100),])
        bands=file_list['band']
        paths=file_list['paths']
        file_names=file_list['file_name']
        print bands
        for download_band in list_bands:
            ind_bands, =np.where( bands == download_band)
            print ind_bands
            if len(ind_bands) >= 1:
               for ind_jj in ind_bands:
                   path_jj=(np.asarray(paths))[ind_jj]
                   filename_jj=np.asarray(file_names)[ind_jj]
                   file_img=dir_ii+'/'+filename_jj
             
                   coadd_tile = filename_jj[:12]
                   src_image=grid_input_images + coadd_tile + '/'+filename_jj+'.fz '
                   print coadd_tile
                   cp_command_img = 'ifdh cp -D '+ src_image + dir_ii+'/.'
                   #cp_command_img = 'cp '+ src_image + dir_ii+'/.'
                   if os.path.isfile(file_img)==0:
                      if os.path.isfile(file_img+'.fz')==1:
                         os.system('rm -f '+file_img + '.fz')
                      os.system(cp_command_img)
                   if os.path.isfile(file_img+'.fz')==1:
                      os.system('funpack -D '+file_img + '.fz')
                   if os.path.isfile(file_img)==1:
                      file_image=file_img+'[0]'
                      file_mask=file_img+'[1]'
                      file_wht=file_img+'[2]'
                      
                      # convolving weights with mask so that masks are considered when stacking
                      hdulist = pyfits.open(file_img)
                      prihdr=hdulist[0].header
                      img=hdulist[0].data
                      mask=hdulist[1].data
                      wt=hdulist[2].data

                      ind_mask=np.where(mask > 0)
                      wt[ind_mask]=0
                      new_hdu_img = pyfits.PrimaryHDU(img, header=prihdr)
                      new_hdu_msk = pyfits.ImageHDU(mask)
                      new_hdu_wt =  pyfits.ImageHDU(wt)
                      new_hdulist=pyfits.HDUList([new_hdu_img, new_hdu_msk, new_hdu_wt])
                      new_hdulist.writeto(file_img, clobber=True)
                      t1=time.time()
                      print "finished download images", t1-t0
                   else:
                      print 'not successful in downloading image '+filename_jj
                      os.system('rm -f '+file_img)
                   
      
