# diffuselight_dataproc

Data processing code for masking, stacking DES Y3 images etc. 

*Dependencies*

python 2.7+, Fermigrid

These code are specifically developed for Fermigrid. Make changes with extreme caution. 
Contact Yuanyuan Zhang(ynzhang@fnal.gov) before getting started

Eventurally the files will be ran as 
divide_files.py example.ini file
(I've included act.ini as an example with a ACT cluster catalog)

the code in the src folder will be ran as
download_files.py example.ini index
index is the index of a cluster in the cluster catalog.

