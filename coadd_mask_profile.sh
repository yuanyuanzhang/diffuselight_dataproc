#!/bin/bash

# some old example code
#USAGE: jobsub_submit -G des -M --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC,OFFSITE --OS=SL6 --expected-lifetime=1h file:///data/des21.a/data/hgaitsch/testthing/HallieExampleNEW.sh
#PART 1: Initial Checks/Setups
#if [ $# -lt 1 ]; then
#echo "Error, you must specify an input directory"
#exit 1
#fi

echo "Job starting on `hostname`"
echo "PWD is $PWD." # You should not try doing a cd to some /data/desXX or /pnfs area, ever.
echo "Home is $HOME"
export HOME=$PWD
echo "PWD is $PWD." # You should not try doing a cd to some /data/desXX or /pnfs area, ever.
echo "Home is $HOME"


/cvmfs/grid.cern.ch/util/cvmfs-uptodate /cvmfs/des.opensciencegrid.org
# check that the DES CVMFS repository is actually there 
if [ ! -d /cvmfs/des.opensciencegrid.org ]; then
echo "DES CVMFS repository is not present. I will quit."
exit 1
fi

# do the various setups
#setup some fermigrid tools that I don't really know, but was told to use
source /cvmfs/des.opensciencegrid.org/eeups/startupcachejob21i.sh
export IFDH_CP_MAXRETRIES=2
export IFDH_XROOTD_EXTRA="-f -N"
export XRD_REDIRECTLIMIT=255
export IFDH_CP_UNLINK_ON_ERROR=1

#setup necessary tools
export PATH=/cvmfs/des.opensciencegrid.org/fnal/anaconda2/bin:$PATH
source activate default
setup swarp
export LD_LIBRARY_PATH=/cvmfs/des.opensciencegrid.org/eeups/fnaleups/Linux64/extralibs/1.0/libs:${LD_LIBRARY_PATH}
export PATH=/cvmfs/des.opensciencegrid.org/2015_Q2/eeups/SL6/eups/packages/Linux64/cfitsio/3.370+0/bin:${PATH}

#-----------------------------------------------------------------------
#part 2 do some actual works

# reading in input
echo "I have received $# input: $@"
if [ $# -lt 6 ]; then
echo "Error, you didn't specify the right number of inputs"
exit 1
fi
listname="$1"
incat="$2"
savdir="$3"
cwd="$4"
inifile="$5"
servicesini="$6"

# copying the necessary files
ifdh cp $listname ./list.txt || { echo "Error copying list ; I will quit" ; exit 1; }
ifdh cp  $inifile  ./inifile.ini || { echo "Error copying ini file; I will quit" ; exit 1; }
ifdh cp -D $incat  ./ || { echo "Error copying catalog file; I will quit" ; exit 1; }
ifdh cp  $servicesini  $HOME/.desservices.ini || { echo "Error copying services file; I will quit" ; exit 1; }
ifdh cp -D "$cwd"/src/download_files_y3.py ./ || { echo "Error copying download_files_y3.py ; continuing on" ; exit 2; }
ifdh cp -D "$cwd"/src/download_images_y3.py ./ || { echo "Error copying download_images_y3.py ; continuing on" ; exit 2; }
ifdh cp -D "$cwd"/src/stack_images.py ./ || { echo "Error copying stack_images.py ; continuing on" ; exit 2; }
ifdh cp -D "$cwd"/src/mask_img.py ./ || { echo "Error copying mask_img.py ; continuing on" ; exit 2; }
ifdh cp -D "$cwd"/src/make_profile.py ./ || { echo "Error copying make_profile.py ; continuing on" ; exit 2; }
ifdh cp -D "$cwd"/src/make_profile_unmasked.py ./ || { echo "Error copying make_profile.py ; continuing on" ; exit 2; }
echo "current content"
ls -lart

while read -r line
do
    num="$line"
    echo "Working on Object $num "

    python download_files_y3.py inifile.ini $num
    python download_images_y3.py inifile.ini $num
    python stack_images.py inifile.ini $num
    python mask_img.py inifile.ini $num
    python make_profile.py inifile.ini $num
    python make_profile_unmasked.py inifile.ini $num
    ifdh mkdir "$savdir"object_"$num"
    ifdh cp -D ./object_"$num"/profile_"$num"_*.txt "$savdir"object_"$num"/. || { echo "Error copying profile output!" ; }
    ifdh cp -D ./object_"$num"/profile_unmasked_"$num"_*.txt "$savdir"object_"$num"/. || { echo "Error copying unmasked profile output!" ; }
    ifdh cp -D ./object_"$num"/coadd* "$savdir"object_"$num"/. || { echo "Error copying image output files!" ; }
done < list.txt #"$filename"
echo "after running I have those files"
ls -lart
