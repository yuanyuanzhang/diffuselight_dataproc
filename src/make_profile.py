import math
import numpy as np
import astropy.io.fits as pyfits
import os
from numpy import cos
from astropy.wcs import WCS
from numpy import cos
from numpy import sin
import sys
import ConfigParser
import time
t0=time.time()

ini_file=sys.argv[1]
ii_ind=int(sys.argv[2])

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
catalog=config.get('input', 'catalog_file')
save_dir='./'

bands=[ii.strip() for ii in config.get('parameters', 'bands').split(',')]
ra_col=config.get('input', 'ra')
dec_col=config.get('input', 'dec')
cat=pyfits.open(catalog)[1].data

for download_band in bands:
 dir_ii=save_dir+'object_%i'%ii_ind
 file_img=dir_ii+'/coadd_masked_%i_'%ii_ind+download_band+'.fits'
 if os.path.isfile(file_img):
    hdu=pyfits.open(file_img)
    prihdr=hdu[0].header
    img=hdu[0].data
    wt=hdu[1].data
    print 'running make_profile.py'
    nx, ny=(prihdr['NAXIS1'], prihdr['NAXIS2'])
    x = np.arange(nx)
    y = np.arange(ny)
    xv, yv = np.meshgrid(x, y)
    dis_max=float(max(nx, ny)-1)/2.0
    cen_x=float(nx-1)/2.0
    cen_y=float(ny-1)/2.0

    dis=np.arange(0, dis_max, 5)
    xx= (dis[1:]+dis[0:len(dis)-1])/2.0
    med=np.zeros(len(dis)-1)
    men=np.zeros(len(dis)-1)
    dis=dis**2

    for ii in range(len(dis)-1):
        dis_center=((xv-cen_x)**2 + (yv-cen_y)**2)
        ind1, ind2=np.where( (dis_center >= dis[ii]) & (dis_center < dis[ii+1]) & (wt>0.001) )
        men[ii]=np.mean(img[(ind1, ind2)])
        med[ii]=np.median(img[(ind1, ind2)])

    z = np.array(zip(xx, men, med), dtype=[('distance', float), ('med', float), ('median', float)] )
    np.savetxt(dir_ii+'/profile_%i_'%ii_ind+download_band+'.txt', z, fmt='%f %f %f')
    t1=time.time()
    print 'finished make_profile.py', t1-t0

