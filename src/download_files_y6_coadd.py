import math
import numpy as np
import astropy.io.fits as pyfits
import os
import easyaccess as ea
import os.path
from numpy import cos
import ConfigParser
import sys
import time
t0=time.time()

ini_file=sys.argv[1]
dbh =ea.connect()
cursor = dbh.cursor()

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
catalog=config.get('input', 'catalog_file')
save_dir='./'

img_dis=float(config.get('parameters', 'img_dis'))
cat_dis=float(config.get('parameters', 'cat_dis'))
bands=[jj.strip() for jj in config.get('parameters', 'bands').split(',')]
ra_col=config.get('input', 'ra')
dec_col=config.get('input', 'dec')

ii_ind=int(sys.argv[2])
cat=pyfits.open(catalog)[1].data
print ra_col, dec_col, ii_ind
print "running download_files_y6_coadd.py"
if 1:
    ra_ii=cat[ii_ind][ra_col]
    dec_ii=cat[ii_ind][dec_col]
    dis_selection=img_dis
    ra_lo=ra_ii-dis_selection/cos(dec_ii/180.0*math.pi)
    ra_hi=ra_ii+dis_selection/cos(dec_ii/180.0*math.pi)
    dec_lo=dec_ii-dis_selection
    dec_hi=dec_ii+dis_selection
    dir_ii=save_dir + 'object_%i'%ii_ind
    if not os.path.exists(dir_ii):
       os.system('ifdh mkdir '+dir_ii)
       print ra_ii, dec_ii, dir_ii
    
 
    #f.write('#filename, pfw_attempt_id,  band, path, mag_zero, pfw_attempt_id, ccd, filename, accepted, flag, expnum \n')
    file_out=dir_ii+'/image_list.csv'
    sql_command="select i.filename, i.pfw_attempt_id, i.band, f.path, i.mag_zero, f.compression from Y6A1_COADD i, Y6A1_proctag t, y6a1_file_archive_info f where t.tag='Y6A1_COADD' and i.filetype='coadd_nobkg' and i.racmin < %f and i.racmax > %f and i.deccmin < %f and i.deccmax > %f and t.pfw_attempt_id = i.pfw_attempt_id and i.filename=f.filename"% (ra_hi, ra_lo, dec_hi, dec_lo)     
    if ra_hi > 360.0:  ### needs to be fixed here. 
       ra_hi_temp= ra_hi - 360.0
       sql_command="select i.filename, i.pfw_attempt_id, i.band, f.path, i.mag_zero, f.compression from Y6A1_COADD i, Y6A1_proctag t, y6a1_file_archive_info f where t.tag='Y6A1_COADD' and i.filetype='coadd_nobkg' and ((i.racmax > %f) or (i.racmin > %f) or (i.racmin < %f) or (i.racmax < %f) or ((i.racmin < %f) and (i.racmax > %f)) ) and i.deccmin < %f and i.deccmax > %f and t.pfw_attempt_id = i.pfw_attempt_id and i.filename=f.filename"% (ra_lo, ra_lo, ra_hi_temp, ra_hi_temp, ra_lo, ra_hi_temp, dec_hi, dec_lo)     
    if ra_lo < 0.0:  ### needs to be fixed here. 
       ra_lo_temp= ra_lo + 360.0
       sql_command="select i.filename, i.pfw_attempt_id, i.band, f.path, i.mag_zero, f.compression from Y6A1_COADD i, Y6A1_proctag t, y6a1_file_archive_info f where t.tag='Y6A1_COADD' and i.filetype='coadd_nobkg' and ((i.racmax > %f) or (i.racmin > %f) or (i.racmin < %f) or (i.racmax < %f) or  ((i.racmin < %f) and (i.racmax > %f)) ) and i.deccmin < %f and i.deccmax > %f and t.pfw_attempt_id = i.pfw_attempt_id and i.filename=f.filename"% (ra_lo_temp, ra_lo_temp, ra_hi, ra_hi, ra_lo_temp, ra_hi, dec_hi, dec_lo)     
    #print sql_command 
    if os.path.isfile(file_out)== 1 :
       print 'temporary file list existed for object %i. Deleted.'%ii_ind    
       os.system('rm -f '+file_out)
    if os.path.isfile(file_out)== 0 :
       dbh.query_and_save(sql_command, file_out)


 
    #sql the coadd catalog
    dis_cat_selection=cat_dis
    ra_cat_lo=ra_ii-dis_cat_selection/cos(dec_ii/180.0*math.pi)
    ra_cat_hi=ra_ii+dis_cat_selection/cos(dec_ii/180.0*math.pi)
    dec_cat_lo=dec_ii-dis_cat_selection
    dec_cat_hi=dec_ii+dis_cat_selection
    sql_cmd="select o.ra, o.dec, o.a_image, o.b_image, o.kron_radius, o.theta_j2000, o.mag_auto_g, o.mag_auto_r, o.mag_auto_i, o.mag_auto_z, o.magerr_auto_i, o.magerr_auto_z, o.class_star_i, o.spread_model_i, o.spreaderr_model_i  from Y6A1_COADD_OBJECT_SUMMARY o where o.ra > %f and o.ra < %f and o.dec >%f and o.dec < %f "%(ra_cat_lo, ra_cat_hi, dec_cat_lo, dec_cat_hi)
    if ra_cat_hi > 360.0:
       ra_cat_hi_temp = ra_cat_hi - 360.0
       sql_cmd="select o.ra, o.dec, o.a_image, o.b_image, o.kron_radius, o.theta_j2000, o.mag_auto_g, o.mag_auto_r, o.mag_auto_i, o.mag_auto_z, o.magerr_auto_i, o.magerr_auto_z, o.class_star_i, o.spread_model_i, o.spreaderr_model_i  from Y6A1_COADD_OBJECT_SUMMARY o where (o.ra > %f or o.ra < %f) and o.dec >%f and o.dec < %f "%(ra_cat_lo, ra_cat_hi_temp, dec_cat_lo, dec_cat_hi)
    if ra_cat_lo <0.0:
       ra_cat_lo_temp = ra_cat_lo + 360.0
       sql_cmd="select o.ra, o.dec, o.a_image, o.b_image, o.kron_radius, o.theta_j2000, o.mag_auto_g, o.mag_auto_r, o.mag_auto_i, o.mag_auto_z, o.magerr_auto_i, o.magerr_auto_z, o.class_star_i, o.spread_model_i, o.spreaderr_model_i  from Y6A1_COADD_OBJECT_SUMMARY o where (o.ra > %f or o.ra < %f) and o.dec >%f and o.dec < %f "%(ra_cat_lo_temp, ra_cat_hi, dec_cat_lo, dec_cat_hi)

    cat_out=dir_ii+'/coadd_catalog.csv'
    if os.path.isfile(cat_out)== 1 :
       print 'coadd object existed for object %i. Deleted.'%ii_ind 
       os.system('rm -f '+cat_out)
    if os.path.isfile(cat_out)== 0 :
       dbh.query_and_save(sql_cmd, cat_out) 
    if os.path.isfile(cat_out)== 0 :
       print 'error did not successfully download any coadd objects, check ra dec %f %f'%(ra_ii, dec_ii)   
    t1=time.time() 
    print "finished download_files_y6_coadd.py", t1-t0




