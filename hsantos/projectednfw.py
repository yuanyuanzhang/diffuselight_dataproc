
# coding: utf-8
# # Testing the projected NFW code for redMaPPer clusters:


get_ipython().magic('matplotlib inline')
import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.3)
from astropy.table import Table


clusters = Table.read('/home/hillysson/diffuselight_dataproc/dlight/input/y1a1_gold_1.0.3-d10-mof-001d_run_redmapper_v6.4.17-vlim_lgt20_desformat_catalog.fit')
members = Table.read('/home/hillysson/diffuselight_dataproc/dlight/input/y1a1_gold_1.0.3-d10-mof-001d_run_redmapper_v6.4.17-vlim_lgt20_desformat_catalog_members.fit')



########### MCMC #############

import emcee
import corner
import scipy.optimize as op

def lnlike(theta, x, y, yerr, R200):
    m, b = theta
    values = [x, R200]
    model = np.array(projectednfw(values, m, b))
    sigma2 = yerr**2 + (model**2) #the variance
    return -0.5*np.sum((y-model)**2/sigma2 + np.log(2*np.pi*sigma2)) # Maximum likelihood estimation

def lnprior(theta):
    m, b = theta
    if 0.0 < m < 10.0 and 0.0 < b < 1.5:
        return 0.0
    return -np.inf

def lnprob(theta, x, y, yerr, R200):
    lp = lnprior(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(theta, x, y, yerr,R200)

def MCMC_coefs(x, y, yerr, R200):
    
    rho0_t, RS_t = 60., 0.35
    
    nll = lambda *args: -lnlike(*args)
    result = op.minimize(nll, [rho0_t, RS_t], args=(x, y, yerr, R200))

    rho_ml, rs_ml = result["x"]

    ndim, nwalkers = 2, 32 # number of free parameters - number be like 2**n
    pos = [result["x"] + 1e-4*np.random.randn(ndim) for i in range(nwalkers)]

    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=(x, y, yerr, R200))

    sampler.run_mcmc(pos, 500)
   
    samples = sampler.chain[:, 50:, :].reshape((-1, ndim))
    
    onesigma_A0 = np.percentile(samples[:,0], [16,50,84])
    onesigma_B0 = np.percentile(samples[:,1], [16,50,84])
   
    return onesigma_A0[1], onesigma_B0[1] 


def criticaldens(z): # Critical density function  
    from astropy import units as u
    SolarM = u.Msun # solar masses
    Mpc3 = u.Mpc * u.Mpc * u.Mpc # Mpc^3
    Conv_factor = SolarM / Mpc3
    rhoc_ = cosmo.critical_density(z)
    rhoc = rhoc_.to(Conv_factor)
    return rhoc.value

def projectednfw(values, rho_s, r_s): # Projected NFW profile function 
    r, r200 = values
    x = (r * r200)/r_s
    N_r = []
    for i in range(len(r)):        
        if x[i] > 1.:
            g_x = (2./np.sqrt(x[i]**2 - 1.)) * np.arctan(  np.sqrt( (x[i]-1.)/(x[i]+1.) ) ) + np.log(x[i]/2.)
        if x[i] < 1.:
            g_x = (2./np.sqrt(1. - x[i]**2)) * np.arctanh( np.sqrt( (1.-x[i])/(x[i]+1.) ) ) + np.log(x[i]/2.)
        if x[i] == 1.:
            g_x = 1. + np.log(x[i]/2.)
        N_up = 4. * np.pi * rho_s * (r_s**3) * g_x  
        if i >= 1:
            A_true = np.pi * (r[i]**2 - r[i-1]**2 )  
            N_r_ = ( N_up -  N_low) * (1./A_true)      
            N_r.append(N_r_)
        N_low = N_up
    return N_r

def rm_mrr(Lambda): #McClintock relation
    X_piv, z_piv = 40., 0.35
    A0, B0, C0, Z0 = 14.489,  1.356, -0.30, 0.275
    return (10.**A0) * ((Lambda/ X_piv)**B0)  * (((1+Z0) / (1+z_piv)) **(C0))

def criticaldens(z): # Critical density function  
    from astropy import units as u
    SolarM = u.Msun # solar masses
    Mpc3 = u.Mpc * u.Mpc * u.Mpc # Mpc^3
    Conv_factor = SolarM / Mpc3
    rhoc_ = cosmo.critical_density(z)
    rhoc = rhoc_.to(Conv_factor)
    return rhoc.value

def plot_projectednfw(R1, R2, Er, F1, F2, R200, coef1, coef2, LAM, CG): # function for plotting the project nfw profiles
    fig = plt.figure(figsize=(4,3)) 
    plt.subplot(1,1,1)
    ax = fig.add_subplot(1,1,1)

    plt.xscale('log')
    plt.yscale('log')
    plt.plot(F1/R200, projectednfw([F2, R200], coef1, coef2), '-',
            label = "Projected NFW Profile")
    plt.errorbar(R1, R2, yerr=Er,fmt='.',color='blue',
                 ecolor='blue', alpha= 0.6, markersize=3)

    plt.annotate('$\lambda$ = '+str(LAM),
                 xy=(1.05,0.88),xycoords='axes fraction',
                 fontsize=10)
    plt.annotate('Cg = '+ '%.2f'%(CG),
                 xy=(1.05,0.78),xycoords='axes fraction',
                 fontsize=10)

    plt.ylabel('$\Sigma $', fontsize=15)  
    plt.xlabel('$R / R_{200} $', fontsize=15)   

    plt.tight_layout()
    ax.spines['top'].set_linewidth(1)
    ax.spines['bottom'].set_linewidth(1)
    ax.spines['left'].set_linewidth(1)
    ax.spines['right'].set_linewidth(1)  
    ax.xaxis.set_tick_params(width=1)
    ax.yaxis.set_tick_params(width=1)
    plt.tick_params(labelsize=15,which='major', length=8,width=1)
    plt.tick_params(which='minor', length=4,width=1)
    plt.yticks(fontsize = 10)
    plt.xticks(fontsize = 10)
    plt.xlim(0.003, 1.)
    plt.ylim(.1, 1000)
    plt.show()
    

def denspro_r200(gdcen, gpmem, lambda_, r200):    # Density profile function
    from scipy.optimize import curve_fit
    import scipy as sy
    
    # bins in radius
    num_gdcenbins = 40
    gdcenmin = 0.01 #min(gdcen)#
    gdcenmax = 1.5 #max(gdcen)#
    #gdcenbins = np.linspace(gdcenmin, gdcenmax, num_gdcenbins)
    gdcenbins = np.logspace(np.log10(gdcenmin), np.log10(gdcenmax), num_gdcenbins)
    gdcenbins = gdcenbins[1:]   
    num_gdcenbins = num_gdcenbins-1
    
    fit_numbers = np.linspace(gdcenmin, gdcenmax, num=10*num_gdcenbins) 
    
    
    profile_pmem = []
    erro_pmem = []
    # looping on radius for this richness bin
    for j in range(num_gdcenbins-1):                
        dcenmask = (gdcen>=gdcenbins[j])*(gdcen<gdcenbins[j+1])
        g_dcen = gdcen[dcenmask]
        g_pmem = gpmem[dcenmask] 

        # area
        R = gdcenbins[j+1]
        r = gdcenbins[j]
        area = np.pi * (R**2 - r**2)
        
        # for simplicity, just counting and normalizing by number of galaxies
        density_pmem = (sum(g_pmem)*1.0)/(area)
        profile_pmem.append(density_pmem) 

        # Poisson fluctuations
        POISSON_pmem = np.sqrt(density_pmem)
        erro_pmem.append(POISSON_pmem)
        
    profile_pmem = np.array(profile_pmem)
    erro_pmem = np.array(erro_pmem)
        
    #Scaling to r200
    radiusr200 = np.array(gdcenbins/r200)
    densr200 = np.array(profile_pmem/r200)
    error200 = np.array(erro_pmem/r200)
    fit_numbersr200 = fit_numbers/r200
        


    # getting the coeficients of best-fit  
    #NFW_coef, NFW_cov = curve_fit(projectednfw, [gdcenbins, r200], profile_pmem, maxfev=100000)
    #print(NFW_coef[0], NFW_coef[1])
    
    # getting the coeficients of best-fit  (MCMC method)
    NFW_coef = MCMC_coefs(gdcenbins, profile_pmem, erro_pmem, r200)
    #samples = NFW_coef[3]
    #print(NFW_coef[0], NFW_coef[1])
    
    cg = r200/NFW_coef[1]
    
    
    plot_projectednfw(radiusr200[:-1], profile_pmem, erro_pmem, fit_numbers[:-1], fit_numbers, r200, NFW_coef[0], NFW_coef[1], lambda_, cg)
    
    return cg


zmask = (clusters['Z_LAMBDA']>=0.2) * (clusters['Z_LAMBDA']<0.35)
clusters=clusters[zmask]
print(len(clusters))

#pm = members['P']*members['PFREE']*members['THETA_R']*members['THETA_I']
pm = members['P']

#Pcrit = (pm > 0.8)

Lambda = []
cg = []
oo = 0
for clid in clusters['MEM_MATCH_ID']:
    
    # members: pmem and center distance
    clusterid = (members['MEM_MATCH_ID'] == clid)
    gpmem = pm[clusterid]
    gdcen = members['R'][clusterid]
    
    # clusters: m200 and r200
    cl_id = list(clusters['MEM_MATCH_ID']).index(clid)
    #lambda_= np.sum(gpmem)   
    lambda_= clusters['LAMBDA_CHISQ'][cl_id]
    m200 = rm_mrr(np.array(lambda_))  
    rhoc = criticaldens(clusters['Z_LAMBDA'][cl_id])
    r200 = np.cbrt( (3.*m200) / (2000.*np.pi*rhoc) ) 
    
    # clusters: concentration
    cg_ = denspro_r200(gdcen, gpmem, lambda_,r200)
    
    Lambda.append(lambda_)
    cg.append(cg_)
    
    oo = oo+1
    print(oo)
    
Lambda = np.array(Lambda)
cg = np.array(cg)
#print(len(cg)) 



# In[79]:


fig = plt.figure(figsize=(4,3))  
ax = plt.subplot(1,1,1)
plt.grid(alpha=0.2)
#plt.xscale('log')
#plt.yscale('log')

plt.scatter(Lambda, cg, c='grey', s=8, alpha=0.5)

plt.xlim(20, 200)
#plt.ylim(.1, 300)
plt.xlabel('$\lambda$', fontsize=13)
plt.ylabel('Concentration (C$_{g}$)', fontsize=13)
plt.tick_params(direction='out',which='major', length=3, width=1, labelsize=0.,
               bottom='on', top='off', left='on', right ='on')
plt.tick_params(direction='out',which='minor', length=2, width=1, labelsize=0,
               bottom='on', top='off', left='on', right ='on')
plt.yticks(fontsize=10)
plt.xticks(fontsize=10)
#[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
#plt.legend(bbox_to_anchor=(0.02, 0.98), loc=2, fontsize=10,borderaxespad=0.,numpoints=1,scatterpoints=1)

plt.show()


z02_035 = Table([Lambda, cg], names=('lambda', 'cg'))
z02_035.write('conc_z02_035.fits', format='fits')


clusters = Table.read('/home/hillysson/diffuselight_dataproc/dlight/input/y1a1_gold_1.0.3-d10-mof-001d_run_redmapper_v6.4.17-vlim_lgt20_desformat_catalog.fit')
members = Table.read('/home/hillysson/diffuselight_dataproc/dlight/input/y1a1_gold_1.0.3-d10-mof-001d_run_redmapper_v6.4.17-vlim_lgt20_desformat_catalog_members.fit')



def denspro_r200_stacked(gdcen, gpmem, lambda_, r200,numcl):    # Density profile function
    from scipy.optimize import curve_fit
    import scipy as sy
    
    # bins in radius
    num_gdcenbins = 10
    gdcenmin = 0.01 #min(gdcen)#
    gdcenmax = 1.5 #max(gdcen)#
    #gdcenbins = np.linspace(gdcenmin, gdcenmax, num_gdcenbins)
    gdcenbins = np.logspace(np.log10(gdcenmin), np.log10(gdcenmax), num_gdcenbins)
    gdcenbins = gdcenbins[1:]   
    num_gdcenbins = num_gdcenbins-1
    
    fit_numbers = np.linspace(gdcenmin, gdcenmax, num=10*num_gdcenbins) 
    
    
    profile_pmem = []
    erro_pmem = []
    # looping on radius for this richness bin
    for j in range(num_gdcenbins-1):                
        dcenmask = (gdcen>=gdcenbins[j])*(gdcen<gdcenbins[j+1])
        g_dcen = gdcen[dcenmask]
        g_pmem = gpmem[dcenmask] 

        # area
        R = gdcenbins[j+1]
        r = gdcenbins[j]
        area = np.pi * (R**2 - r**2)
        
        # for simplicity, just counting and normalizing by number of galaxies
        density_pmem = (sum(g_pmem)*1.0)/(numcl*area)
        profile_pmem.append(density_pmem) 

        # Poisson fluctuations
        POISSON_pmem = np.sqrt(density_pmem)
        erro_pmem.append(POISSON_pmem)
        
    profile_pmem = np.array(profile_pmem)
    erro_pmem = np.array(erro_pmem)
        
    #Scaling to r200
    radiusr200 = np.array(gdcenbins/r200)
    densr200 = np.array(profile_pmem/r200)
    error200 = np.array(erro_pmem/r200)
    fit_numbersr200 = fit_numbers/r200
        
    # getting the coeficients of best-fit  
    #NFW_coef, NFW_cov = curve_fit(projectednfw, [gdcenbins, r200], profile_pmem, maxfev=100000)
    #print(NFW_coef[0], NFW_coef[1])
    
    # getting the coeficients of best-fit  (MCMC method)
    NFW_coef = MCMC_coefs(gdcenbins, profile_pmem, erro_pmem, r200)
    #samples = NFW_coef[3]
    #print(NFW_coef[0], NFW_coef[1])
    
    cg = r200/NFW_coef[1]
    #if cg <0.:
    plot_projectednfw(radiusr200[:-1], profile_pmem, erro_pmem, fit_numbers[:-1], fit_numbers, r200, NFW_coef[0], NFW_coef[1], lambda_, cg)
    
    return cg



