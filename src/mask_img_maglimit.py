import math
import numpy as np
import astropy.io.fits as pyfits
import os
import os.path
from numpy import cos
from astropy.wcs import WCS
from numpy import cos
from numpy import sin
import sys
import ConfigParser

ini_file=sys.argv[1]
ii_ind=int(sys.argv[2])

config = ConfigParser.ConfigParser()
inifile = config.read(ini_file)[0]
catalog=config.get('input', 'catalog_file')
save_dir='./'

img_dis=float(config.get('parameters', 'img_dis'))
cat_dis=float(config.get('parameters', 'cat_dis'))
mag_lim=float(config.get('parameters', 'mag_lim'))
magerr_lim=float(config.get('parameters', 'magerr_lim'))
npixels=int(config.get('parameters', 'pixel_dis'))*2
bands=[ii.strip() for ii in config.get('parameters', 'bands').split(',')]
ra_col=config.get('input', 'ra')
dec_col=config.get('input', 'dec')


cat=pyfits.open(catalog)[1].data
print ii_ind

for download_band in bands:

    ra_ii=cat[ii_ind][ra_col]
    dec_ii=cat[ii_ind][dec_col]
    dir_ii=save_dir+'object_%i'%ii_ind
    file_img=dir_ii+'/coadd_%i_'%ii_ind+download_band+'.fits'
    coadd_file=dir_ii+'/coadd_catalog.csv'
    print coadd_file, file_img
    if os.path.isfile(coadd_file) and os.path.isfile(file_img):

        # read in coadd catalog, read out objects to be masked
        coadd_obj=np.loadtxt(coadd_file, skiprows=1, usecols=(0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11), delimiter=',', dtype=[('ra', float), ('dec', float), ('a_image', float), ('b_image', float), ('kron_radius', float), ('THETA_J2000', float), ('mag_auto_r', float), ('mag_auto_i', float), ('mag_auto_z', float), ('magerr_auto_i', float), ('magerr_auto_z', float)])    
        #ind_purge, =np.where(( coadd_obj['mag_auto_i'] < 22.4 ) & ( coadd_obj['magerr_auto_i'] <0.218 ) )    
        #ind_purge, =np.where(( coadd_obj['mag_auto_z'] < 20.51 ) & ( coadd_obj['magerr_auto_z'] <0.72 ) )    
        ind_purge, =np.where(( coadd_obj['mag_auto_z'] < mag_lim ) & ( coadd_obj['magerr_auto_z'] <magerr_lim ) )    
        coadd_obj=coadd_obj[ind_purge] 
      
        ## work on masking images 
        w = WCS(file_img)
        x_ii, y_ii=w.all_world2pix(ra_ii, dec_ii, 1)
        coadd_ra=coadd_obj['ra']
        coadd_dec=coadd_obj['dec']
        ratios_coadd=coadd_obj['a_image']/coadd_obj['b_image']
        pos_angs_coadd=coadd_obj['THETA_J2000']
        x_coadd, y_coadd=w.all_world2pix(coadd_ra, coadd_dec, 1)
        dis_coadd=np.sqrt((x_coadd-x_ii)**2+(y_coadd-y_ii)**2)
        #rad_coadd=coadd_obj['a_image']*coadd_obj['kron_radius']
        rad_coadd=coadd_obj['a_image']*coadd_obj['kron_radius']*1.4
        mag_r=coadd_obj['mag_auto_i']

        hdulist = pyfits.open(file_img)
        prihdr=hdulist[0].header
        newheader=prihdr
        #rotate positional angles
        rotate_angle=math.atan(prihdr['CD1_2']/prihdr['CD1_1'])*180.0/math.pi   
        print 'check, the following value should be close to 0:', rotate_angle 
        # read in image and weight file#
        img=hdulist[0].data
        wt=hdulist[1].data
        # read in background and subtract from img
        #hdulist_bkg = pyfits.open(file_bkg)
        #average_bkg = np.median(hdulist_bkg[0].data)
        #img = img - average_bkg
        # null the image and the weight file#
        xx_ind=np.arange(0, prihdr['NAXIS1'])
        yy_ind=np.arange(0, prihdr['NAXIS2']) 

        nx, ny=(prihdr['NAXIS1'], prihdr['NAXIS2'])
        x = np.arange(nx)
        y = np.arange(ny)
        xv, yv = np.meshgrid(x, y)
        ind_coadd, =np.where( (x_coadd < nx+rad_coadd) & (x_coadd + rad_coadd > 0) & (y_coadd < ny+rad_coadd) & (y_coadd+rad_coadd>0) & (dis_coadd > 5) )
        for ind_kk in range( len(ind_coadd) ):
            ratio_temp=ratios_coadd[ind_coadd[ind_kk]]
            posang_temp=pos_angs_coadd[ind_coadd[ind_kk]]+rotate_angle+90.0# added 90.0 because of using theta_j2000
            posang_temp=posang_temp*math.pi/180.0
            x_dist_src=(xv-x_coadd[ind_coadd[ind_kk]])*cos(posang_temp)+(yv-y_coadd[ind_coadd[ind_kk]])*sin(posang_temp)
            y_dist_src=-(xv-x_coadd[ind_coadd[ind_kk]])*sin(posang_temp)+(yv-y_coadd[ind_coadd[ind_kk]])*cos(posang_temp)
            dist_src= x_dist_src**2 + (y_dist_src**2)*(ratio_temp**2) 
            #check x_dist_src, y_dist_src if there should be a minus sign. 
            #dist_src = ( xv-x_coadd[ind_coadd[ind_kk]] )**2+( yv-y_coadd[ind_coadd[ind_kk]] )**2
            dis_compare=rad_coadd[ind_coadd[ind_kk]]**2
            
            ##fix the stellar distance thing with dis_min ##
            dis_min= 48.0-2.0*mag_r[ind_coadd[ind_kk]]
            if dis_min > 0:
               dis_min=dis_min**2
            else:
               dis_min=0
            
            ##Fix this kron_radius =0 thing##
            if dis_compare < 0.1:
               dis_compare= (coadd_obj[ind_coadd[ind_kk]]['a_image']*10)**2
            if dis_compare < dis_min:
               dis_compare=dis_min

            ind_temp =np.where(dist_src < dis_compare)
            img[ind_temp]=0
            wt[ind_temp]=0
        new_hdu_img = pyfits.PrimaryHDU(img, header=newheader)
        new_hdu_wt =  pyfits.ImageHDU(wt)
        new_hdulist=pyfits.HDUList([new_hdu_img, new_hdu_wt])
        new_hdulist.writeto(dir_ii+'/coadd_masked_%i_'%ii_ind+download_band+'.fits', clobber=True)

