
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# ## Lupton Magnitudes
# 
# https://iopscience.iop.org/article/10.1086/301004
# 
# https://arxiv.org/pdf/astro-ph/9903081.pdf

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table


# In[2]:


for i in range(4):  
    
    fig = plt.figure(figsize=(8,6))  
    ax = plt.subplot()
    plt.xscale('log')

    plt.grid(alpha=0.4)


    colors = ['C1','C2','C3','C4']
    
    ngal_bins=i+1

    richness_dir = ['sub_moments_20_30.fits',
                    'sub_moments_30_45.fits',
                    'sub_moments_45_60.fits',
                    'sub_moments_60_150.fits']
    str_ngals = ['$\lambda$ = [20;30)',
                 '$\lambda$ = [30;45)',
                 '$\lambda$ = [45;60)',
                 '$\lambda$ = [60;150)']
    dir_ind='/home/hillysson/diffuselight_dataproc/'
    
    
    jacknife_profile=np.loadtxt(dir_ind+richness_dir[i])
    r_red, mea, std, mag_unc = jacknife_profile.T[0], jacknife_profile.T[1], jacknife_profile.T[2], jacknife_profile.T[3]

    
    ############################
    ## traditional magnitudes ##
    ############################
    
    
    flux_up = mea+std
    flux_low = mea-std
    
    mag = 30 - 2.5 * np.log10(mea)
    mag_up = 30 - 2.5 *np.log10(flux_up)
    mag_low = 30 - 2.5 * np.log10(flux_low)
            
    plt.plot(r_red, mag, '-', lw=1.5,alpha=0.6,color=colors[i],label=str_ngals[i])
    plt.fill_between(r_red, mag_up, mag_low, color=colors[i], alpha=0.15, label='')
    
    
    ##########################
    ## computing luptitudes ##
    ##########################
    
    a =  1.08574
    f0 = 10.**(30./-2.5)
    sigma = std
    flux = mea
    
    b = 1.042*sigma
    b_ = f0 * b
    m0 = 2.5*np.log10(f0)
    sigma_ = f0 * sigma
    
    
    mu = (30. - 2.5 * np.log10(b_)) - a * np.arcsinh(flux/ (2.*b_) ) 
    #mu_up = (30. - 2.5 * np.log10(b_)) - a * np.arcsinh(flux_up/ (2.*b_) )
    #mu_low = (30. - 2.5 * np.log10(b_)) - a * np.arcsinh(flux_low/ (2.*b_) )
    
    var_mag_unc = ( (a)**2 * (sigma_)**2 ) / ( 4*(b_)**2 + (f0)**2) 
    mag_unc = np.sqrt( var_mag_unc )   
         
    plt.plot(r_red, mu, '-', lw=1.5,alpha=0.6,color='black',label='luptitude')
    #plt.fill_between(r_red, mu_up, mu_low, color=colors[i], alpha=0.15, label='')
    plt.fill_between(r_red, mu + mag_unc, mu - mag_unc, color='black', alpha=0.15, label='')
    
    plt.ylabel('$\mu_{r}$ [mag Kpc$^{-2}$]', fontsize=17)
    plt.xlabel('r [Kpc]', fontsize=17)

    plt.xticks(fontsize = 0)
    plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
                   bottom='on', top='on', left='on', right ='on')
    plt.tick_params(direction='in',which='minor', length=3, width=2, labelsize=0.,
                   bottom='on', top='on', left='on', right ='on')
    plt.yticks(fontsize=15)
    plt.xticks(fontsize=15)
    plt.ylim(40,20)
    plt.xlim(2,1500)
    [ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
    plt.subplots_adjust(wspace=0, hspace=0)

    plt.legend(bbox_to_anchor=(0.06, 0.38), loc=2, fontsize=12,borderaxespad=0.,numpoints=1,scatterpoints=1)

    plt.show()
    ##############################################################################


# In[ ]:




