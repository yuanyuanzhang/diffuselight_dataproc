
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 

# In[1]:


import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
import os
import os.path
import configparser
import sys


# In[2]:


ini_file= 'rand_y1.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir=config.get('output', 'save_dir')
grid_output_dir=config.get('grid_output', 'save_dir')
rand=pyfits.open(catalog)[1].data
flags=np.zeros(len(rand))+1
indices=np.arange(len(rand))


for file_flag in flag_files:
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        if len(flag) == len(flags):
            flags=flags*flag
ind, =np.where( (flags > 0.5) & (flags<1.5) )
print(len(ind), len(indices))


# In[3]:


####
second_path = 'fermigrid_rand/'
dat_type=[('distance', float), ('mean', float), ('median', float)]
data_temp=np.loadtxt(input_dir+second_path+'object_%i/profile_%i_r.txt'%(ind[10],ind[10]), dtype=dat_type)
ncluster=len(ind)
nbins=len(data_temp['distance'])#20#399
yy_g=np.zeros([nbins, ncluster])+99999
yy_r=np.zeros([nbins, ncluster])+99999
yy_g_med=np.zeros([nbins, ncluster])+99999
yy_r_med=np.zeros([nbins, ncluster])+99999
xx=data_temp['distance']
ct=0

for jji, jj in enumerate(ind):

    
    file_jj_g=input_dir+second_path+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir+second_path+'object_%i/profile_%i_r.txt'%(jj,jj)
    #print(os.path.isfile(file_jj_r))
    if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
        data_g=np.loadtxt(file_jj_g, dtype=dat_type)
        data_r=np.loadtxt(file_jj_r, dtype=dat_type)
        #print(len(data_r['distance']))
        #print(data_r[:2])
        

        if len(data_g['distance'])==nbins and len(data_r['distance'])==nbins:
            if 1:
                yy_g[:, jji]=data_g['mean']
                yy_g_med[:, jji]=data_g['median']
                yy_r[:, jji]=data_r['mean']
                yy_r_med[:, jji]=data_r['median']
                ct=ct+1
                #print(jji, len(ind))

print('total number of points to use:', ct)


# In[4]:


yy=np.zeros(nbins)
for ii in range(nbins):
    print(yy_r[ii, :])
    arr_temp =yy_r[ii, :]
    ind_temp, =np.where((arr_temp < 30000) & (arr_temp >-30000))
    arr=arr_temp[ind_temp]
    yy[ii]=np.mean(arr)


# In[5]:


## jacknife sampling
coord_rand=np.transpose( np.array([rand[ind]['ra'], rand[ind]['dec']]) )
ncen = 40
if os.path.isfile('centers.txt') == 0:
    km = kmeans_sample(coord_rand, ncen, maxiter=100, tol=1.0e-5)
    print("found centers:",km.centers)
    print("converged?",km.converged)
    np.savetxt('centers.txt', km.centers)
centers=np.loadtxt('centers.txt')
labels_rand=kmeans_radec.find_nearest(coord_rand, centers)
len_kmeans=centers.shape[0]

dir_ind=save_dir+'rand_y1_jacknife_rand/'
os.system('mkdir '+dir_ind)


for ii in range(len_kmeans):
    ind_rand,=np.where( (labels_rand != ii) )
    np.savetxt(dir_ind+'jacknife_index_%i.txt'%ii, ind[ind_rand], fmt='%i') 
    print(ii, len(ind_rand))
    

    
    yygmean_ind=np.zeros(nbins)
    yygmed_ind=np.zeros(nbins)
    yyrmean_ind=np.zeros(nbins)
    yyrmed_ind=np.zeros(nbins)
    for jj in range(nbins):
        temp_g_mean =yy_g[jj, :]
        temp_g_med =yy_g_med[jj, :]
        temp_r_mean =yy_r[jj, :]
        temp_r_med =yy_r_med[jj, :]
        
        ind_temp, =np.where((temp_g_mean < 30000) & (temp_g_mean >-30000) &                             (temp_g_med < 30000) & (temp_g_med >-30000) &                             (temp_r_mean < 30000) & (temp_r_mean >-30000) &                             (temp_r_med < 30000) & (temp_r_med >-30000) &                             #(labels_rand != ii) )
                            (labels_rand == ii) )
        arr=temp_g_mean[ind_temp]
        yygmean_ind[jj]=np.mean(arr)
        arr=temp_g_med[ind_temp]
        yygmed_ind[jj]=np.mean(arr)
        
        arr=temp_r_mean[ind_temp]
        yyrmean_ind[jj]=np.mean(arr)
        arr=temp_r_med[ind_temp]
        yyrmed_ind[jj]=np.mean(arr)
        
    z=list(zip(xx, yygmean_ind, yygmed_ind, yyrmean_ind, yyrmed_ind))
    #z=list(zip(xx, yyrmean_ind, yyrmed_ind))
    np.savetxt(dir_ind+'jacknife_profile_%i.txt'%ii, z, fmt='%f, %f, %f, %f, %f', header='# xx, g_mean, g_median, r_mean, r_median')
    #np.savetxt(dir_ind+'jacknife_profile_%i.txt'%ii, z, fmt='%f, %f, %f', header='# xx, r_mean, r_median')


# ### ICL stacked profiles comparison 

# In[6]:


from astropy.table import Table
new_profile= Table.read(dir_ind+'jacknife_profile_%i.txt'%2, format='ascii')
new_profile.colnames


# In[32]:


fig = plt.figure(figsize=(7,7))  
plt.subplot(1,1,1)
ax = fig.add_subplot(1,1,1)
plt.xscale('log')


maxflux = 0
yregionsprofiles = []
for i in range(40):
    new_profile = Table.read(dir_ind+'jacknife_profile_%i.txt'%i, format='ascii')
    #new_profilename = (((new_path.split('/')[-1]).split('.')[0]).replace('_', ' ')).replace('profile', 'HS')
    x = new_profile['# xx'] *(0.263/60.)
    y = new_profile['r_mean'] /(0.263**2)      
    yregionsprofiles.append(y)   
    if max(y)>maxflux:
        maxflux = max(y)
        index_ = i   


del yregionsprofiles[index_]
ymeanjacknife = np.array(yregionsprofiles)
means = [np.mean([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymeanjacknife.T]
std_  = [np.std([iii for iii in sublist if ~np.isnan(iii)] or 0) for sublist in ymeanjacknife.T]
np.savetxt('moments_rand.txt', np.array([x, means, std_]).T, fmt='%f', header='# # xx, means, std_')   

for i in range(40):
    
    if i == index_:
        continue
    new_profile = Table.read(dir_ind+'jacknife_profile_%i.txt'%i, format='ascii')
    #new_profilename = (((new_path.split('/')[-1]).split('.')[0]).replace('_', ' ')).replace('profile', 'HS')
    x = new_profile['# xx'] *(0.263/60.)
    y = new_profile['r_mean'] /(0.263**2)   
    
    plt.plot(x, y, '-', lw=2,alpha=0.1,color='k',label='region %s'%(i))
    

plt.plot(x, np.array(means), '-', lw=1.5,alpha=0.6,color='r',label='mean')
plt.plot(x, np.array(means)+np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.plot(x, np.array(means)-np.array(std_), '-', lw=1.5,alpha=0.6,color='r',label='std')
plt.ylabel('flux [arcsec$^{-2}$]', fontsize=17)
plt.xlabel('r [arcmin]', fontsize=17)
#plt.xlim(0.0, 0.8)
#plt.ylim(0.0, 0.8)

plt.xticks(fontsize = 0)
plt.tick_params(direction='in',which='major', length=6, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.tick_params(direction='in',which='minor', length=0, width=2, labelsize=0.,
               bottom='on', top='on', left='on', right ='on')
plt.yticks(fontsize=15)
plt.xticks(fontsize=15)
[ax.spines[axis].set_linewidth(2.) for axis in ['top','bottom','left','right']]
plt.subplots_adjust(wspace=0, hspace=0)
    
#plt.legend(bbox_to_anchor=(1.05, 0.9), loc=2, fontsize=17,borderaxespad=0.,numpoints=1,scatterpoints=1)
plt.show()

