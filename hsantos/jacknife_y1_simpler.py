
# coding: utf-8

# # Intra-Cluster Light (ICL) profiles analysis in the Dark Energy Survey (DES)
# 
# 
# 

# In[23]:


import healpy as hp
import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
from healpy.rotator import Rotator
import kmeans_radec
from kmeans_radec import KMeans, kmeans_sample
from astropy.table import Table
import os
import os.path
import configparser
import sys
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.3)


# In[24]:


ini_file= 'redmapper_y1_new.ini'
config = configparser.ConfigParser()
inifile = config.read(ini_file)
input_dir_rdmp=config.get('input', 'input_dir')
catalog=config.get('input', 'catalog_file')
flag_files_rdmp=[ii.strip() for ii in config.get('input', 'flag_files').split(',')]
save_dir_rdmp=config.get('output', 'save_dir')
grid_output_dir_rdmp=config.get('grid_output', 'save_dir')
rdmp=pyfits.open(catalog)[1].data
flags_rdmp=np.zeros(len(rdmp))+1
indices_rdmp=np.arange(len(rdmp))

str_ngals = ['$\lambda$ = [20;30)',
             '$\lambda$ = [30;45)',
             '$\lambda$ = [45;60)',
             r'$\lambda$ = [60;$\infty$)']
richness_dir = ['richness_20_30/',
                'richness_30_45/',
                'richness_45_60/',
                'richness_60_beyond/']

# getting indices in the flag files
for file_flag in flag_files_rdmp:
    if os.path.isfile(file_flag):
        flag_dat=np.loadtxt(file_flag, dtype=[('index', int), ('flag', int)], skiprows=1)
        flag=flag_dat['flag']
        if len(flag) == len(flags_rdmp):
            flags_rdmp=flags_rdmp*flag

ind1, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 20) & (rdmp['lambda_chisq'] < 30))
print(len(ind1), len(indices_rdmp))
ind2, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 30) & (rdmp['lambda_chisq'] < 45))
print(len(ind2), len(indices_rdmp))
ind3, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 45) & (rdmp['lambda_chisq'] < 60))
print(len(ind3), len(indices_rdmp))
ind4, =np.where( (flags_rdmp > 0.5) & (flags_rdmp<1.5) & (rdmp['lambda_chisq']>= 60) & (rdmp['lambda_chisq'] < 40000000))
print(len(ind4), len(indices_rdmp))


# In[43]:


#inds = [ind1,ind2,ind3,ind4]
#for ij in range(4):
#    ngal_bins=ij
#    ind = inds[ngal_bins-1]
#    print('z = '+str(np.mean(rdmp['Z'][ind])))
    
ngal_bins=4
inds = [ind1,ind2,ind3,ind4]
ind = inds[ngal_bins-1]
Ncl = len(ind)
richness_dir = ['richness_20_30/',
                'richness_30_45/',
                'richness_45_60/',
                'richness_60_beyond/']

####
second_path_rdmp = 'fermigrid_profiles_new/'
dat_type=[('distance', float), ('mean', float), ('median', float)]

to_be_removed = []

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id
    
    #file_jj_g=input_dir_rdmp+second_path_rdmp+'object_%i/profile_%i_g.txt'%(jj,jj)
    file_jj_r=input_dir_rdmp+second_path_rdmp+'object_%i/profile_%i_r.txt'%(jj,jj)
    #if os.path.isfile(file_jj_g) and os.path.isfile(file_jj_r):
    if os.path.isfile(file_jj_r):
        continue
    else:
        print('there is no profile for the cluster  with index '+str(jj))
        to_be_removed.append(jj)

# remove the clusters indices that there is no profiles
for rem in to_be_removed:
    ind = np.delete(ind, list(ind).index(rem))
    
print('total number of clusters to use: '+str(len(ind)))
        


# In[44]:


####
#x = new_profile['# xx']
#y = new_profile['r_mean'] 

dir_sub=save_dir_rdmp+'subtracted_profiles/'+richness_dir[ngal_bins-1]
dat_type=[('distance', float), ('mean', float), ('median', float)]
data_temp=Table.read(dir_sub+'subtracted_profile_%i.txt'%ind[10], format='ascii')
ncluster=len(ind)
nbins=len(data_temp['# xx'])#20#399
yy_r=np.zeros([nbins, ncluster])+999999
xx=data_temp['# xx']
ct=0

for jji, jj in enumerate(ind):  # jji is an index from 0 to 356
                                # jj is the cluster id

    file_jj=dir_sub+'subtracted_profile_%i.txt'%jj
    if os.path.isfile(file_jj):
        data_sub=Table.read(file_jj, format='ascii')
        
        if len(data_sub['# xx'])==nbins: # nbins: length of the profile, number of radius bins
            yy_r[:, jji]=data_sub['r_mean']
            ct=ct+1
        else:
            print(jji)


print('total number of clusters to use: '+str(ct))
        


# In[45]:


## jacknife sampling
from astropy.io.fits import Column

# getting the center position of rdmp clusters
coord_rdmp=np.transpose( np.array([rdmp[ind]['ra'], rdmp[ind]['dec']]) )
ncen = 40

if os.path.isfile('phy_centers.txt') == 0: #this is false, so the code is jumping this part
    km = kmeans_sample(coord_rdmp, ncen, maxiter=100, tol=1.0e-5)
    print("found centers:",km.centers)
    print("converged?",km.converged)
    np.savetxt('phy_centers.txt', km.centers)
centers=np.loadtxt('phy_centers.txt')

# find the rdmp cluster closest to the center of the region:
labels_rdmp=kmeans_radec.find_nearest(coord_rdmp, centers)
print(labels_rdmp)
print(str(len(labels_rdmp))+' clusters')
print('  ')


len_kmeans=centers.shape[0] # 40, number of regions
os.system('mkdir '+save_dir_rdmp+'redmapper_y1_jacknife_sub_profiles/')
dir_ind=save_dir_rdmp+'redmapper_y1_jacknife_sub_profiles/'+richness_dir[ngal_bins-1]
os.system('mkdir '+dir_ind)

c0=Column(name='Jacknife_index', format='K')
c1=Column(name='x_axis', format='%iE'%nbins)
c2=Column(name='mean', format='%iE'%nbins)
cols = pyfits.ColDefs([c0, c1, c2])
hdu = pyfits.BinTableHDU.from_columns(cols, nrows=len_kmeans)

for ii in range(len_kmeans): # binning in kmean regions
    
    # subtracting clusters that belong to this region
    ind_rdmp,=np.where( (labels_rdmp != ii) )    
    print('region %i:'%(ii))
    print(str(len(labels_rdmp)-len(ind_rdmp))+' cluster(s) were removed')
    print(str(len(ind_rdmp))+' cluster(s) were used')
    print('')
    
    yyrmean_ind=np.zeros(nbins)    
    for jj in range(nbins): # binning in radius        
        temp_r_mean =yy_r[jj, :]        
        ind_temp, =np.where((temp_r_mean < 30000) & (temp_r_mean >-30000) &                             (labels_rdmp != ii) )        
        arr=temp_r_mean[ind_temp]
        yyrmean_ind[jj]=np.mean(arr)
        
    hdu.data[ii]['Jacknife_index']=ii
    hdu.data[ii]['x_axis']=xx
    hdu.data[ii]['mean']=yyrmean_ind

hdu.writeto(dir_ind+'jacknife_profile_r_proper_kpc.fits')
    #z=list(zip(xx, yyrmean_ind)) 
    #np.savetxt(dir_ind+'jacknife_sub_profile_%i.txt'%ii, z, fmt='%f,%f', header='# xx,r_mean')



# In[46]:


def jacknife_statistics(dat1, dat2 = np.zeros(1)):

    njack, narray = dat1.shape
    if dat1.shape != dat2.shape:
        dat2 = np.zeros([njack, narray]) # alternatively dat2 is 0
        print('no second array, using 0')
    mean = np.zeros(narray); err = np.zeros(narray)
    err_factor = np.sqrt(njack-1)

    for ii in range(narray):
        bkg = 0 #dat1[:, narray-1] - dat2[:, narray-1] # 0 for default
        mean[ii] = np.mean(dat1[:, ii] - dat2[:, ii]-bkg)
        err[ii] = np.std(dat1[:, ii] - dat2[:, ii] - bkg) * err_factor
    return mean, err

rdmp_jack=pyfits.open(dir_ind+'jacknife_profile_r_proper_kpc.fits')
mean, err=jacknife_statistics(rdmp_jack[1].data['mean'])
xx_arr = rdmp_jack[1].data['x_axis']

yyfactor=1.0/(0.263*cosmo.kpc_proper_per_arcmin(0.275).value/60.0)**2
np.savetxt(dir_ind+'sub_moments.txt', np.array([xx_arr[0], mean*yyfactor, err*yyfactor]).T, fmt='%f', header='# # xx, means, std')   


